# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <string.h>
# include <time.h>
# include <sys/select.h>
# include <gsl/gsl_errno.h>
# include <gsl/gsl_spline.h>

// LCM Includes
# include "Man2Cont_Man2Cont_t.h"
# include "fau_coordinate_t.h"

// local includes
# include "global.h"
# include "RXManeuvering.h"

int main( void )
{
// total number of waypoints
# define N 20

  // local includes
  int i,j,msg;
  int n; // number of waypoints in trajectory
  double t[N];
  double y[N];
  double x[N];
  
  ////////////////////////////
  //INITIALIZE GSL VARIABLES//
  ////////////////////////////
  
  gsl_interp_accel * xacc = gsl_interp_accel_alloc(); // accelerator lookup 
  gsl_interp_accel * yacc = gsl_interp_accel_alloc(); // accelerator lookup 
  gsl_spline * xspline; // north spline 
  gsl_spline * yspline; // east spline
  
  // allocate spline to arbitrary number of points 
  xspline = gsl_spline_alloc(gsl_interp_cspline, 3);
  yspline = gsl_spline_alloc(gsl_interp_cspline, 3);
  
  // evaluation of polynomial
  double xt, yt, ut, vt;
  
  //////////////////////////////
  //TRAJECTORY CHECK VARIABLES//
  //////////////////////////////
  
  // trajectory check variables to check for repeat trajectories
  int trajTF = 1;
  int oldN;
  float * oldTraj;
  float * newTraj;
  
  // allocate a large amont of memory for both oldTrah and newTraj
  oldTraj = (float *)malloc(100*sizeof(float));
  newTraj = (float *)malloc(100*sizeof(float));
  
  ///////////////////////
  //TIMESTAMP VARIABLES//
  ///////////////////////
  
  long double orig, curr;
  double diff;
  
  // timeout structure
  struct timeval timeout = {0, 5}; // 5 microsecond wait time
  
  #ifdef _PRINT
  FILE * LogFile;
  LogFile = fopen("Points.txt","a");
  #endif
  
  //////////////////////////////
  //PRINTOUT START UP MESSAGES//
  //////////////////////////////
  printf("\n\n\tFLORIDA ATLANTIC UNVIERSTIY (c) 2014\n");
  printf("\tROBOTX CHALLENGE - SINGAPORE BAY - 2014\n\n\n");
  printf("\tSTARTING MANEUVERING EXECUTABLE\n\n");
  printf("\tTIME AT START (UTC): %lf\n", GetTimeStamp());
  #ifdef _DEBUG
  printf("\tDEBUG: ON\n\n\n");
  #endif 
  #ifndef _DEBUG
  printf("\tDEBUG: OFF\n\n\n");
  #endif
  
  
  ///////////////////////
  //LCM INITIALIZATIONS//
  ///////////////////////
  
  // to controller
  lcm_t * ToController = lcm_create("udpm://239.255.76.67:7667?ttl=1");
  if(!ToController)
  {
	printf("LCM ToController channel failure\n");
	return 1;
  }
  
  // initialize LCM struct
  Man2Cont_Man2Cont_t ToControllerStruct;
  
  // from planner
  lcm_t * FromPlanner = lcm_create("udpm://239.255.76.67:7667?ttl=1");
  if(!FromPlanner)
  {
	printf("LCM FromPlanner channel failure\n");
	return 1;
  }
  
  // initialize LCM struct
  fau_coordinate_t_subscription_t * FromPlannerPtr;
  fau_coordinate_t FromPlannerStruct;
  
  // subscribe
  FromPlannerPtr = fau_coordinate_t_subscribe(FromPlanner, "goto", &goto_Handler, &FromPlannerStruct);
	
  // set queue capacity to one, store only latest capacity
  fau_coordinate_t_subscription_set_queue_capacity(FromPlannerPtr, 1);
  
  printf("\tLCM INITIALIZATIONS: SUCCESS\n\n\n");
  
  /////////////////////////
  //RECEIVE FIRST MESSAGE//
  /////////////////////////
  
  printf("\tWAITING FOR INITIAL MESSAGE FROM PLANNER\n\n");
 
 // handle first message
  lcm_handle(FromPlanner);
  
  // store old variables
  oldN = FromPlannerStruct.count;
  
  // store old trajectory
  oldTraj = (float *) realloc(oldTraj,oldN*sizeof(float));
  memcpy(oldTraj,FromPlannerStruct.traject,oldN*sizeof(float));
  
  // store also as new trajectory
  newTraj = (float *) realloc(newTraj,FromPlannerStruct.count*sizeof(float));
  memcpy(newTraj,FromPlannerStruct.traject,FromPlannerStruct.count*sizeof(float));
  
  printf("\tFIRST MESSAGE RECEIVED. STARTING MANEUVERING EXECUTABLE\n");
  
  switch(FromPlannerStruct.mode)
  {
  case 0:
	  
	  // delete previous object
	  gsl_spline_free(yspline);
	  gsl_spline_free(xspline);
  
	  // store number of waypoints
	  n = FromPlannerStruct.count/3;
	  
	  ///////////////////////////////
	  //UPDATE TO CONTROLLER STRUCT//
	  ///////////////////////////////
	  
	  ToControllerStruct.mode = 0;
	  
	  #ifdef _DEBUG
		printf("\tFIRST MESSAGE RECEIVED ON CHANNEL: 'goto' \n\n");
		printf("\tMESSAGE MODE: TRAJECTORY TRACKING CONTROLLER\n");
		printf("\t\tX \t  Y \t T \n");
		for(i=0; i<FromPlannerStruct.count; i+=3)
			printf("\t[\t%f\t%f\t%f\t]\n",FromPlannerStruct.traject[i],FromPlannerStruct.traject[i+1],FromPlannerStruct.traject[i+2]);
			printf("\n\n");
	  #endif
	  
	  // error in message format
	  if (n < 2 || FromPlannerStruct.count%3 != 0)
	  {
		printf("\tERROR: TRAJECTORY MISMATCH. NOT ENOUGH POINTS IN TRAJECTORY.\n"); 
	
		// north
		x[0] = 0; 
		x[1] = 0;
		x[2] = 0;
		
		// east
		y[0] = 0; 
		y[1] = 0;
		y[2] = 0;
		
		// time
		t[0] = 0;
		t[1] = 10;
		t[2] = 20;	
		
		n = 3;
		
		// override control mode and tell vehicle to stop
        ToControllerStruct.mode = 2;
	    ToControllerStruct.PortCmd = 0;
	    ToControllerStruct.StbdCmd = 0;
	  }
	  // less than three waypoints in trajectory
	  else if( n == 2)
	  {
		// store in x structure
		x[0] = FromPlannerStruct.traject[0];
		x[2] = FromPlannerStruct.traject[3];
		
		// store in y structure
		y[0] = FromPlannerStruct.traject[1];
		y[2] = FromPlannerStruct.traject[4];
		
		// store in time structure
		t[0] = FromPlannerStruct.traject[2];
		t[2] = FromPlannerStruct.traject[5];
		
		// create intermediate point
		x[1] = (x[0]+x[2])/2.0;
		y[1] = (y[0]+y[2])/2.0;
		t[1] = (t[0]+t[2])/2.0;
		
		// update n
		n = 3;
	  }
	  
	  // proper formatting for waypoints with 3 or more waypoints
      else
	  {
		  
		  // store coordinates in appropriate structure
		  
		  // north
		  j = 0;
		  for(i=0; i<FromPlannerStruct.count; i+=3)
		  {
			x[j] = (double)FromPlannerStruct.traject[i];
			j++;
		  }
		  
		  // east
		  j = 0;
		  for(i=1; i<FromPlannerStruct.count; i+=3)
		  {
			y[j] = (double)FromPlannerStruct.traject[i];
			j++;
		  }
		  
		  // time
		  j = 0;
		  for(i=2; i<FromPlannerStruct.count; i+=3)
		  {
			t[j] = (double)FromPlannerStruct.traject[i];
			j++;
		  }
	  }
	  
	  #ifdef _DEBUG
			printf("\tSTORED VALUE IN MATRICES: \n\n");
			for(i=0; i<n; i++)
				printf("\t[\t%f\t%f\t%f\t]\n",x[i],y[i],t[i]);
			printf("\n\n");
	  #endif
	  
	  ////////////////////////////////////////////////
	  //Allocate Memory According to Trajectory Size//
	  ////////////////////////////////////////////////
	  
	  xspline = gsl_spline_alloc (gsl_interp_cspline, n);
	  yspline = gsl_spline_alloc (gsl_interp_cspline, n);
	  
	  
	  /////////////////////////
	  //CALCUATE HERMITE POLY//
	  /////////////////////////
	  
	  gsl_spline_init (xspline, t, x, n);
	  gsl_spline_init (yspline, t, y, n);
	  
	  break;
	  
  case 1:
	  
	 #ifdef _DEBUG
		printf("\tFIRST MESSAGE RECEIVED ON CHANNEL: 'goto' \n\n");
		printf("\tMESSAGE MODE: STATIONKEEPING CONTROLLER\n");
		printf("\t\tX \t  Y \t PSI \n");
			printf("\t[\t%f\t%f\t%f\t]\n",FromPlannerStruct.traject[0],FromPlannerStruct.traject[1],FromPlannerStruct.traject[2]);
			printf("\n\n");
	  #endif
	  
	  // store in proper structure
	  if( FromPlannerStruct.count == 3)
	  {
	    ToControllerStruct.mode = 1;
	    ToControllerStruct.sk_x = FromPlannerStruct.traject[0];
	    ToControllerStruct.sk_y = FromPlannerStruct.traject[1];
	    ToControllerStruct.sk_psi = FromPlannerStruct.traject[2];
	  }
	  // error in sending, override and tell vehicle to stop
	  else
	  {
	    ToControllerStruct.mode = 2;
	    ToControllerStruct.PortCmd = 0;
	    ToControllerStruct.StbdCmd = 0;
	  }
	  
	  Man2Cont_Man2Cont_t_publish(ToController, "MAN2CONT", &ToControllerStruct);
	  break;
	  
  case 2:
  
	  #ifdef _DEBUG
		printf("\tFIRST MESSAGE RECEIVED ON CHANNEL: 'goto' \n\n");
		printf("\tMESSAGE MODE: DIRECT CONTROL\n");
		printf("\t\tPort \t  Stbd \n");
			printf("\t[\t%f\t%f\t]\n",FromPlannerStruct.traject[0],FromPlannerStruct.traject[1]);
			printf("\n\n");
	  #endif
	  
      // store in proper structure 
	  if( FromPlannerStruct.count == 2 )
	  {
	    ToControllerStruct.mode = 2;
	    ToControllerStruct.PortCmd = FromPlannerStruct.traject[0];
	    ToControllerStruct.StbdCmd = FromPlannerStruct.traject[1];
	  }
	  else 
	  {
	    ToControllerStruct.mode = 2;
	    ToControllerStruct.PortCmd = 0;
	    ToControllerStruct.StbdCmd = 0;
	  }
	  
	  Man2Cont_Man2Cont_t_publish(ToController, "MAN2CONT", &ToControllerStruct);
	  break;
	  
	default:
		printf("\tCONTROL MODE = %d\n",FromPlannerStruct.mode);
		printf("\tERROR INVALID CONTROL MODE!\n\n");
		
		// override and tell vehicle to stop
	    ToControllerStruct.mode = 2;
	    ToControllerStruct.PortCmd = 0;
	    ToControllerStruct.StbdCmd = 0;
		break;
	}
	  
  /////////////////
  //GET TIMESTAMP//
  /////////////////
  
  orig = GetTimeStamp();
  
   while(1)
   {
	  // check for LCM Message
      msg = LCMCheckForMessage(FromPlanner, &timeout);
	  if (msg == 1)
	  {
		  lcm_handle(FromPlanner);
		  
		  // reset new trajectory variables
		  newTraj = (float *)realloc(newTraj,FromPlannerStruct.count*sizeof(float));
		  memcpy(newTraj,FromPlannerStruct.traject,FromPlannerStruct.count*sizeof(float));
		  
		  // check to see if this is a repeat of the trajectory
		  trajTF = trajectoryCheck(oldTraj,newTraj,oldN,FromPlannerStruct.count);
		  
		  if(trajTF == 1)
		  {
			  // copy data from new trajectory to old trajectory
			  oldN = FromPlannerStruct.count;
			  oldTraj = (float *)realloc(oldTraj,oldN*sizeof(float));
			  memcpy(oldTraj,FromPlannerStruct.traject,oldN*sizeof(float));
		  
			  switch(FromPlannerStruct.mode)
			  {
			  case 0:
				  
				  // delete previous object
				  gsl_spline_free(yspline);
				  gsl_spline_free(xspline);
				
				  // store value in n
				  n = FromPlannerStruct.count/3;
				  
				  ///////////////////////////////
				  //UPDATE TO CONTROLLER STRUCT//
				  ///////////////////////////////
				  
				  ToControllerStruct.mode = 0;
			  
				  // check for number of waypoints in trajectory
				  if (n < 2 || FromPlannerStruct.count%3 != 0)
				  {
					printf("\tERROR: TRAJECTORY MISMATCH. NOT ENOUGH POINTS IN TRAJECTORY.\n"); 
	
					// dummy waypoints for trajectory
					
					// north
					x[0] = 0; 
					x[1] = 0;
					x[2] = 0;
		
					// east
					y[0] = 0; 
					y[1] = 0;
					y[2] = 0;
		
					// time
					t[0] = 0;
					t[1] = 10;
					t[2] = 20;	
		
					n = 3;
		
					// override control mode and tell vehicle to stop
					ToControllerStruct.mode = 2;
					ToControllerStruct.PortCmd = 0;
					ToControllerStruct.StbdCmd = 0;
				  }
				  else if( n == 2)
				  {
					// store in x structure
					x[0] = FromPlannerStruct.traject[0];
					x[2] = FromPlannerStruct.traject[3];
					
					// store in y structure
					y[0] = FromPlannerStruct.traject[1];
					y[2] = FromPlannerStruct.traject[4];
					
					// store in time structure
					t[0] = FromPlannerStruct.traject[2];
					t[2] = FromPlannerStruct.traject[5];
					
					// create intermediate point
					x[1] = (x[0]+x[2])/2.0;
					y[1] = (y[0]+y[2])/2.0;
					t[1] = (t[0]+t[2])/2.0;
					
					// update n
					n = 3;
				  }
				  else
				  {
					  
					  // store coordinates in appropriate structure
					  
					  // north
					  j = 0;
					  for(i=0; i<FromPlannerStruct.count; i+=3)
					  {
						x[j] = (double)FromPlannerStruct.traject[i];
						j++;
					  }
					  
					  // east
					  j = 0;
					  for(i=1; i<FromPlannerStruct.count; i+=3)
					  {
						y[j] = (double)FromPlannerStruct.traject[i];
						j++;
					  }
					  
					  // time
					  j = 0;
					  for(i=2; i<FromPlannerStruct.count; i+=3)
					  {
						t[j] = (double)FromPlannerStruct.traject[i];
						j++;
					  }
				  }
				  #ifdef _DEBUG
						printf("\tSTORED VALUE IN MATRICES: \n\n");
						for(i=0; i<n; i++)
							printf("\t[\t%f\t%f\t%f\t]\n",x[i],y[i],t[i]);
						printf("\n\n");
				  #endif
				  
				  ////////////////////////////////////////////////
				  //Allocate Memory According to Trajectory Size//
				  ////////////////////////////////////////////////

				  xspline = gsl_spline_alloc (gsl_interp_cspline, n);
				  yspline = gsl_spline_alloc (gsl_interp_cspline, n);
		  
		  
				  /////////////////////////
				  //CALCUATE HERMITE POLY//
				  /////////////////////////
				  
				  gsl_spline_init (xspline, t, x, n);
				  gsl_spline_init (yspline, t, y, n);
				  
				  break;
			  case 1:
			  
			  #ifdef _DEBUG
				  printf("\tMESSAGE MODE: STATIONKEEPING CONTROLLER\n");
				  printf("\t\tX \t  Y \t PSI \n");
				  printf("\t[\t%f\t%f\t%f\t]\n",FromPlannerStruct.traject[0],FromPlannerStruct.traject[1],FromPlannerStruct.traject[2]);
				  printf("\n\n");
			  #endif
		  
				// store in proper structure
				if( FromPlannerStruct.count == 3)
				{
				  ToControllerStruct.mode = 1;
				  ToControllerStruct.sk_x = FromPlannerStruct.traject[0];
				  ToControllerStruct.sk_y = FromPlannerStruct.traject[1];
				  ToControllerStruct.sk_psi = FromPlannerStruct.traject[2];
			    }
				// error in sending, override and tell vehicle to stop
				else
				{
				  ToControllerStruct.mode = 2;
				  ToControllerStruct.PortCmd = 0;
				  ToControllerStruct.StbdCmd = 0;
			    }
		  
				  Man2Cont_Man2Cont_t_publish(ToController, "MAN2CONT", &ToControllerStruct);
				  break;
			 
			  case 2:
			   
			   #ifdef _DEBUG
				  printf("\tFIRST MESSAGE RECEIVED ON CHANNEL: 'goto' \n\n");
				  printf("\tMESSAGE MODE: DIRECT CONTROL\n");
				  printf("\t\tPort \t  Stbd \n");
				  printf("\t[\t%f\t%f\t]\n",FromPlannerStruct.traject[0],FromPlannerStruct.traject[1]);
				  printf("\n\n");
				#endif
		  
				// store in proper structure 
				if( FromPlannerStruct.count == 2 )
				{
				  ToControllerStruct.mode = 2;
				  ToControllerStruct.PortCmd = FromPlannerStruct.traject[0];
				  ToControllerStruct.StbdCmd = FromPlannerStruct.traject[1];
				}
				else 
				{
				  ToControllerStruct.mode = 2;
				  ToControllerStruct.PortCmd = 0;
				  ToControllerStruct.StbdCmd = 0;
				}
		  
				  Man2Cont_Man2Cont_t_publish(ToController, "MAN2CONT", &ToControllerStruct);
				  break;
			  default:
				  printf("\tCONTROL MODE = %d\n",FromPlannerStruct.mode);
				  printf("\tERROR INVALID CONTROL MODE!\n\n");
				  
				  // override and tell vehicle to stop
				  ToControllerStruct.mode = 2;
				  ToControllerStruct.PortCmd = 0;
				  ToControllerStruct.StbdCmd = 0;
				  break;
			  }
			  
			  /////////////////
			  //GET TIMESTAMP//
			  /////////////////
			  
			  orig = GetTimeStamp();
		  }
		  
		  // reset flag
		  msg = 0;
	  }
   
	  // get new time
	  curr = GetTimeStamp();
	  diff = (double)(curr-orig);
	  
	  
	  switch(FromPlannerStruct.mode)
	  {
	  
	  case 0:
		  ///////////////////////
		  //EVALUATE POLYNOMIAL//
		  ///////////////////////
		  
		  // x
		  xt = gsl_spline_eval(xspline,diff,xacc);
		  
		  // y
		  yt = gsl_spline_eval(yspline,diff,yacc);
		  
		  // u
		  ut = gsl_spline_eval_deriv(xspline,diff,xacc);
		  
		  // v
		  vt = gsl_spline_eval_deriv(yspline,diff,yacc);
		  
		  #ifdef _DEBUG
		  printf("\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", xt, yt, ut, vt,diff);
		  #endif
		  
		  #ifdef _PRINT
		  fprintf(LogFile,"%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", xt, yt, ut, vt,diff);
		  
		  usleep(100);
		  if(diff > 100.0)
		  {
			fclose(LogFile);
			return 1;
		  }
		  #endif
		  
		  //////////////////////
		  //PUBLISH TO CHANNEL//
		  //////////////////////
		  
		  ToControllerStruct.xt = xt;
		  ToControllerStruct.yt = yt;
		  ToControllerStruct.ut = ut;
		  ToControllerStruct.vt = vt;
		  
		  Man2Cont_Man2Cont_t_publish(ToController, "MAN2CONT", &ToControllerStruct);
		  break;
		  
	  case 1:
		  Man2Cont_Man2Cont_t_publish(ToController, "MAN2CONT", &ToControllerStruct);
		  break;
	  case 2:
		  Man2Cont_Man2Cont_t_publish(ToController, "MAN2CONT", &ToControllerStruct);
		  break;
	  }
		  // wait for 0.25 seconds
		  usleep(250000);
    }

  return 1;
# undef N


}
