# Maneuvering Curve fitting function

This executable is heavily reliant on the GNU Scientific Library (GSL). Make sure that it is installed on computers that will run it.

* Channel : "MAN2CONT"
* Address : 239.255.76.67:7667

* LCM Package: 

```
#!c

package Man2Cont;

struct Man2Cont_t
{
	// mode corresponding to trajectory tracking, stationkeeping, and direct control
	int16_t mode;

	// Trajectory Tracking Mode 0 
    float xt;
	float yt;
	float ut;
	float vt;
	
	// Stationkeeping Mode 1
	float sk_x;
	float sk_y;
	float sk_psi;
	
	// Direct Control Mode 2
	int16_t PortCmd; // from -100 to 100
	int16_t StbdCmd; // from -100 to 100
}
```