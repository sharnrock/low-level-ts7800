
int trajectoryCheck(float * oldTraj, float * newTraj, int oldn, int newn)
{/*
	This function checks to see if two trajectories are equal to each other, and if so
	returns a 0;
	
	Input, newTraj - new trajectory that was just received
	Input, oldTraj - old trajectory that was just received
	Input, oldn - number of points in old trajectory
	Input, newn - number of points in new trajectory
	
	output, tf - 0 same trajectory, 1 new trajectory
*/
	int tf;
	int i = 0;
	
	// check to see if old trajectory has the same number of waypoints as the new one
	if( oldn != newn) return 1;
	
	while( *(oldTraj+i) == *(newTraj+i) && i < newn)
		i++;
	
	if(i == newn) tf = 0;
	else tf = 1;
	
	return tf;
}
  
  /////////////////
  //LCM FUNCTIONS//
  /////////////////
  
static void goto_Handler(const lcm_recv_buf_t *rbuf, const char * channel, const fau_coordinate_t * msg, void * user_data)
{
/*
	This function is used as a handler for the 'MAN2CONT'channel on LCM.
	
	Traditional LCM Inputs:
	
	Input, rbuf - LCM receive buffer
	Input, channel - channel name
	Input, msg - msg received
	Input, user_data - should be same type as msg, transfer all data from this handler to function
*/

	// copy to memory
	//memcpy(user_data,msg,sizeof(fau_coordinate_t));
	
	
	int i;
	// allocate memory
	((fau_coordinate_t *)user_data)->mode = (int16_t *) malloc(sizeof(int16_t));
	((fau_coordinate_t *)user_data)->count =(int16_t *) malloc(sizeof(int16_t));
	((fau_coordinate_t *)user_data)->traject = (float *) malloc(msg->count*sizeof(float));
	
	((fau_coordinate_t *)user_data)->mode = msg->mode;
	((fau_coordinate_t *)user_data)->count = msg->count;
	memcpy(((fau_coordinate_t *)user_data)->traject, msg->traject, msg->count*sizeof(float));
}