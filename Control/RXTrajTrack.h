  //////////////////////
  // CONTROL FUNCTONS //
  //////////////////////

TRAJCONTstruct PDTrajTrackCont(POSEstruct BFE_struct, TRAJGAINstruct gains)
{
/*
	This function calculates the control forces required from errors in position and velocity
	
	Input, BFE_struct - body-fixed error structure BFE_struct.xt, BFE_struct.yt, BFE_struct.ut, BFE_struct.vt
	Input, gains - gains for the PD controller
 
	Output, control_struct - control forces (unlimited)
*/
	TRAJCONTstruct control_struct;
	
	control_struct.X = gains.Kx*BFE_struct.xt + gains.Ku*BFE_struct.ut;
	control_struct.N = gains.Ky*BFE_struct.yt + gains.Kv*BFE_struct.vt;

	return control_struct;
}

TRAJCONTstruct TrajControlLimit(TRAJCONTstruct unlim_struct, float beta)
{
/*
	This function calculates the control forces required from errors in position and velocity
	
	Input, unlim_struct - unlimited control forces
	Input, beta - exponential decay constant, > 0
	
	Output, lim_struct - limited forces
	
*/	
	TRAJCONTstruct lim_struct;
	float thrust_sat;
	
	thrust_sat = Saturation(unlim_struct.X, 0.0,TMAX);
	
	// saturate the steering torque and calculate thust
	lim_struct.N = Saturation(unlim_struct.N, -NMAX, NMAX);
	lim_struct.X = thrust_sat*exp(-beta*fabs(lim_struct.N));

	return lim_struct;
}

THRUstruct TrajControlForces2Thrust(TRAJCONTstruct lim_struct)
{
/*
	This function calculates the required thrust from the port and stbd motors from the control forces.
	
	Input, lim_struct - limited control forces
	
	Output, thrust_struct - thrust for each motor pod
*/	
	
	THRUstruct thrust_struct; // output structure
	float pbar = 1.0; // sidehull separation
	
	thrust_struct.Tp = lim_struct.N/(2.0*pbar) + lim_struct.X/2.0;
	thrust_struct.Ts = -lim_struct.N/(2.0*pbar) + lim_struct.X/2.0;
	
	// modify for saturation
	if(thrust_struct.Tp < NEGMAX/2.0 && thrust_struct.Ts < TMAX/2.0)
		thrust_struct.Ts += (NEGMAX/2.0 - thrust_struct.Tp);
		
	if(thrust_struct.Ts < NEGMAX/2.0 && thrust_struct.Tp < TMAX/2.0)
		thrust_struct.Tp += (NEGMAX/2.0 - thrust_struct.Ts);

	// apply saturation
	thrust_struct.Tp = Saturation(thrust_struct.Tp, NEGMAX/2.0, TMAX/2.0); // only forward
	thrust_struct.Ts = Saturation(thrust_struct.Ts, NEGMAX/2.0, TMAX/2.0); // only forward
	
	return thrust_struct;
}