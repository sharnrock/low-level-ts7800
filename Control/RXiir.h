// Infinite Impulse Response Filter Functions

typedef struct {
	// coefficients
	double b[2]; // b[0] current - b[1] previous input 
	double a; // output gain
	
	// input and output buffers
	double x[2]; // x[0] current - x[1] previous input 
	double y[2]; // y[0] current - y[1] previous output
} iirStruct;

iirStruct iirFilterInit()
{
	// dummy variable
	iirStruct innerStruct;
	
	// clear memory buffers and all variables
	memset(&innerStruct, 0, sizeof(iirStruct));
	
	return innerStruct;
}

void iirFilter(iirStruct * iir)
{
	// move last output to new output
	iir->y[1] = iir->y[0]; // 1 is last, 0 is current
	
	// perform calculations
	iir->y[0] = (iir->b[0])*(iir->x[0]) + (iir->b[1])*(iir->x[1]) - (iir->a)*(iir->y[1]);
	
	// move current input to last input
	iir->x[1] = iir->x[0]; // 1 is last, 0 is current
}

void iirFlushBuffer(iirStruct * iir)
{
	// reset buffers for input and output
	iir->x[0] = 0;
	iir->x[1] = 0;
	iir->y[0] = 0;
	iir->y[1] = 0;
}
