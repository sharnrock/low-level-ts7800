////////////////////////////
//DIRECT CONTROL FUNCTIONS//
////////////////////////////

char MotorPower2Byte(int MotorPower)
{/*
	This function converts a motor power percentage between -100 and 100 to an uint8_t
	
	Input, MotorPower - motor power percentage from -100 to 100
	
	Output, byte - unsigned character relating motor power percentage to byte
*/
	char byte;
	
	byte = (char)(127.0/100.0*MotorPower + 127.0);
	return byte;
}