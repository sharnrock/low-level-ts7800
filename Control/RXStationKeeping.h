// Station Keeping Control Code
//Values necessary to calculate the derivatives
extern	float eta_d_prev[3] = {0.0,0.0,0.0};		//xd_prev = 0, yd_prev = 0, psid_prev = 0;
extern	float eta_t_prev[3] = {0.0,0.0,0.0};		//ex_prev = 0, ey_prev = 0, epsi_prev = 0;
extern	float eta_r_dot_prev[3] = {0.0,0.0,0.0};	//xr_prev = 0, yr_prev = 0, psir_prev = 0;

float HeadingErrorWrap(float desired_heading, float actual_heading)
{/*
	This function calculates the heading error wrap around for the compass IN RADIANS
	
	Input, desired_heading - desired heading IN RADIANS
	Input, actual_heading - actual heading IN RADIANS
	
	Output, error - heading wrapped error IN RADIANS
	
*/	
	float error = desired_heading - actual_heading;
	
	if(error > pi) error -= 2*pi;
	else if(error < -pi) error += 2*pi;	
	
	return error;
}

SKCONTstruct StationKeepingController(POSEstruct BFE_struct, SKGAINstruct gains, float heading_error)
{/*
	This function calculates the control forces required from errors in position and velocity
	
	Input, BFE_struct - body-fixed error structure BFE_struct.xt, BFE_struct.yt, BFE_struct.ut, BFE_struct.vt
	Input, gains - gains for the station keeping controller
	Input, heading_error - wrapped heading error IN RADIANS
 
	Output, control_struct - control forces (unlimited)
*/
	SKCONTstruct control_struct;

	control_struct.X = gains.Kx*BFE_struct.xt + gains.Ku*BFE_struct.ut;
	control_struct.Y = gains.Ky*BFE_struct.yt + gains.Kv*BFE_struct.vt;
	control_struct.N = gains.Kpsi*heading_error;
	
	return control_struct;
}



	/////////////////EDO///////////////////////////////////////////////////////////
//                                                    (skGains,           DesiredPOSE_AltStruct,  curr_pose,           curr_vel,            yaw,                  DesiredPOSE.sk_psi*pi/180.0,CurrentPOSE.USV_pose[2])
SKCONTstruct StationKeepingRobustController(SKGAINstruct gains, POSEstruct desi_struct, NEDstruct curr_pose, BODstruct curr_velocity, float actual_heading, float desired_heading, float yaw_rate)
{/*
	This function calculates the control forces required from errors in position and velocity

	Input, gains - gains for the station keeping controller
	Input, desi_struct - desired position structure (north east down frame) desi_struct.xt, desi_struct.yt
	Input, curr_pose - current position structure (north east down frame) curr_pose.north, curr_pose.east
	Input, curr_velocity - current velocity (body fixed frame) 
	Input, actual_heading - in RADIANS
	Input, desired_heading - in RADIANS
	Input, yaw_rate - (body fixed adjustment ????) in RAD/sec
 
	Output, control_struct - control forces (unlimited)
*/
	SKCONTstruct control_struct;											//Output forces from the controller
	
	float lambda[3] = {0.16, 0.16, 0.16};
	float eta[3] = {curr_pose.north, curr_pose.east, actual_heading};		//Current State
	float eta_d [3] = {desi_struct.xt, desi_struct.yt, desired_heading};	//Desired State
	float eta_d_dot[3];														//Derivative of Desired State
	float eta_t[3];															//Error: Current - Desired
	float eta_t_dot[3];														//Derivative of error
	float eta_r_dot[3];														//Relative
	float eta_r_dot_dot[3];
	float s[3];																//Sliding Surface
	
	//PD gain matrices (J'*Kp and J'Kd)
	float Kp[3][3] = {  
		{gains.Kx*cos(actual_heading), gains.Ky*sin(actual_heading), 0} ,  
		{gains.Kx*(-sin(actual_heading)), gains.Ky*cos(actual_heading), 0} , 
		{0, 0, gains.Kpsi}   
		};
	float Kd[3][3] = {  
		{gains.Ku*cos(actual_heading), gains.Kv*sin(actual_heading), 0} ,  
		{gains.Ku*(-sin(actual_heading)), gains.Kv*cos(actual_heading), 0} ,   
		{0, 0, 50.0}   	
		};
			//CHANGE
	int i;																	//Dummy variable				
	float ts = 0.25;														//Sampling time
	
	//WAM-V 16' parameters
	float m = 149.9;      													//Total mass [kg] (from actual scale measurements)
	float rho = 1025.9;   													//density of seawater [kg/m^3]
	float L = 3.21;       													//Length on the waterline [m]
	float B = 0.36;       													//Demihull beam [m]
	float T = 0.36/2.0;        												//Draft of the demihull [m]
	float p_bar = 1.83/2.0; 												//sidehull separation - from centerplane vessel to centerplane demihull
	float Iz = 239.1369;  				//CHANGE									//Mass moment of inertia about z-axis [kg*m^2] (143.5 original estimate) from solidworks model(x3/2)

	//Transform current velocity from body fixed frame to north east down frame
	NEDstruct NED_curr_velocity = Bod2Geo(curr_velocity, actual_heading);
	
	float u = NED_curr_velocity.north;
	float v = NED_curr_velocity.east;
	float r = yaw_rate;														//yaw rate in radians
	float u_port, u_stbd, D_port, D_stbd;
	float Xudot, Nrdot, Yvdot, U, Yv, Nr;
	float non_dimensional_Yv = -150.0;
	float non_dimensional_Nr = -0.3;
	float Xu = 0.0;
	float P[4] = {0.9479, -7.9117, 43.8586, -0.2644}; 						//coefficients for the drag equation
	float tau_drag[3];
	float M[3][3];
	float C[3][3];
	float D[3][3];
		

	//Controller Algorithm
	for ( i = 0; i < 3; i++ ) {
		eta_t[i] = eta[i] - eta_d[i];
		if (i == 2){
			if(eta_t[2] > pi) eta_t[2] -= 2*pi;
			else if(eta_t[2] < -pi) eta_t[2] += 2*pi;
		}
		eta_d_dot[i] = (eta_d[i] - eta_d_prev[i])/ts;
		eta_r_dot[i] = eta_d_dot[i] - lambda[i]*eta_t[i];
		eta_t_dot[i] = (eta_t[i] - eta_t_prev[i])/ts;
		eta_r_dot_dot[i] = (eta_r_dot[i] - eta_r_dot_prev[i])/ts;
		s[i] = eta_t_dot[i] + lambda[i]*eta_t[i];
    }
	
	
	//Vector location of port and starboard hulls
	
	u_port = u + r*p_bar;
	u_stbd = u - r*p_bar;

	//Added Mass Hydrodynamic Coefficients
	//Estimated for each hull, then multiplied by 2 to account for both hulls
	Xudot = - 0.05*m/2.0;

	//Using strip theory to estimate added mass coefficients
	//Nrdot = - 1*m*(L^2+(2*p_bar)^2)/12+0.5*(0.1*m*(2*p_bar)^2+rho*pi*T^2*L^3)/12;
	Nrdot = - 1.0*m*(L*L+(2*p_bar)*(2*p_bar))/12.0+0.5*(0.1*m*(2*p_bar)*(2*p_bar)+rho*pi*T*T*L*L*L)/12.0;
	Yvdot = - 0.1*rho*pi*T*T*L;

	//Multiply by 2
	Xudot = 2*Xudot;
	Yvdot = 2*Yvdot;

	//Linear Drag Coefficients
	//Calculate Coefficient of Drag for Sway from 1950 SNAME approximation
	U = sqrt(u*u + v*v);
	Yv = non_dimensional_Yv;
	Nr = non_dimensional_Nr*pi*rho*U*T*T*L*L;

	//Nonlinear Drag Terms
	//Drag and Moment due to drag on entire vessel - includes nonlinear and linear drag terms
	D_port = P[0]*u_port*u_port*u_port + P[1]*u_port*u_port + P[2]*u_port;   // Can assume that this equation also takes into account Xu - so set Xu=0
	D_stbd = P[0]*u_stbd*u_stbd*u_stbd + P[1]*u_stbd*u_stbd + P[2]*u_stbd;   // Can assume that this equation also takes into account Xu - so set Xu=0
	
	//Maneuvering Coefficient Matrices
	tau_drag[0] = 0.0;//-(D_port + D_stbd);
	tau_drag[1] = 0.0;
	tau_drag[2] = 0.0;//(D_stbd - D_port)*p_bar;
	
	//Added Mass Matrix * J'
	M[0][0] = (m-Xudot)*cos(actual_heading);
	M[0][1] = (m-Xudot)*sin(actual_heading);
	M[0][2] = 0;
	M[1][0] = -(m-Yvdot)*sin(actual_heading);
	M[1][1] = (m-Yvdot)*cos(actual_heading);
	M[1][2] = 0;
	M[2][0] = 0;
	M[2][1] = 0;
	M[2][2] = (Iz - Nrdot);
	
	//Force matrices * J'
	C[0][0] = 0;
	C[0][1] = 0;
	C[0][2] = -(m-Yvdot)*v;
	C[1][0] = 0;
	C[1][1] = 0;
	C[1][2] = (m-Xudot)*u;
	C[2][0] = (m-Yvdot)*v*cos(actual_heading) + (m-Xudot)*u*sin(actual_heading);
	C[2][1] = (m-Yvdot)*v*sin(actual_heading) - (m-Xudot)*u*cos(actual_heading);
	C[2][2] = 0;
	
	D[0][0] = Xu*cos(actual_heading);
	D[0][1] = Xu*sin(actual_heading);
	D[0][2] = 0;
	D[1][0] = -Yv*sin(actual_heading);
	D[1][1] = Yv*cos(actual_heading);
	D[1][2] = 0;
	D[2][0] = 0;
	D[2][1] = 0;
	D[2][2] = Nr;
	
	
	// Calculate the control 
	control_struct.X = 0.05*(M[0][0]*eta_r_dot_dot[0]+M[0][1]*eta_r_dot_dot[1] + C[0][2]*eta_r_dot[2] + D[0][0]*eta_r_dot[0]+D[0][1]*eta_r_dot[1] + tau_drag[0] - (Kd[0][0]*s[0]+Kd[0][1]*s[1])) - (Kp[0][0]*eta_t[0]+Kp[0][1]*eta_t[1]);
	control_struct.Y = 0.05*(M[1][0]*eta_r_dot_dot[0]+M[1][1]*eta_r_dot_dot[1] + C[1][2]*eta_r_dot[2] + D[1][0]*eta_r_dot[0]+D[1][1]*eta_r_dot[1] + tau_drag[1] - (Kd[1][0]*s[0]+Kd[1][1]*s[1])) - (Kp[1][0]*eta_t[0]+Kp[1][1]*eta_t[1]);
	control_struct.N = 0.05*(M[2][2]*eta_r_dot_dot[2] + C[2][0]*eta_r_dot[0]+C[2][1]*eta_r_dot[1] + D[2][2]*eta_r_dot[2] + tau_drag[2]) - (Kd[2][2]*s[2]) - (Kp[2][2]*eta_t[2]);
	
	eta_d_prev[0] = eta_d[0];	
	eta_d_prev[1] = eta_d[1];
	eta_d_prev[2] = eta_d[2];	
	eta_t_prev[0] = eta_t[0];	
	eta_t_prev[1] = eta_t[1];
	eta_t_prev[2] = eta_t[2];	
	eta_r_dot_prev[0] = eta_r_dot[0];	
	eta_r_dot_prev[1] = eta_r_dot[1];
	eta_r_dot_prev[2] = eta_r_dot[2];
	
	return control_struct;
}
///////////////////////////////////////////////END OF EDO/////////////////////////////////////////////////////////


SKMotorState SKControlAllocation(SKCONTstruct SKControlForces)
{/*
	This function calculates the control forces required from errors in position and velocity
	
	Input, SKControlForces - Forces as given by gains in StationKeepingController()
 
	Output, LimState - limited thrust forces and UNFILTERED angles for motors IN DEGREES
*/
	// transformation matrix
	float T[4][3] = {
		{0.5,  0.762,  0.5},
		{0.0,  0.5,    0.0},
		{0.5, -0.762, -0.5},
		{0.0,  0.5,    0.0}
	};
	
	SKMotorState UnlimState, LimState;
	float ForceArray[4]; // [Fx1 Fy1 Fx2 Fy2]
	
	// apply transformation
	ForceArray[0] = T[0][0]*SKControlForces.X + T[0][1]*SKControlForces.Y + T[0][2]*SKControlForces.N;
	ForceArray[1] = T[1][0]*SKControlForces.X + T[1][1]*SKControlForces.Y + T[1][2]*SKControlForces.N;
	ForceArray[2] = T[2][0]*SKControlForces.X + T[2][1]*SKControlForces.Y + T[2][2]*SKControlForces.N;
	ForceArray[3] = T[3][0]*SKControlForces.X + T[3][1]*SKControlForces.Y + T[3][2]*SKControlForces.N;
	
	// calculate angles using four quadrant arctan function
	UnlimState.PortThrust = sqrt(pow(ForceArray[0],2) + pow(ForceArray[1],2));
	UnlimState.PortAzi = atan2(ForceArray[1],ForceArray[0])*180.0/pi;
	UnlimState.StbdThrust = sqrt(pow(ForceArray[2],2) + pow(ForceArray[3],2));
	UnlimState.StbdAzi = atan2(ForceArray[3],ForceArray[2])*180.0/pi;
	
	// turn off motors if within deadangles
	if((UnlimState.StbdAzi > 45.0 && UnlimState.StbdAzi < 137.5) || (UnlimState.StbdAzi < -42.5 && UnlimState.StbdAzi > -135.0))
		UnlimState.StbdThrust *= 0.1;
		
	if((UnlimState.PortAzi > 45.0 && UnlimState.PortAzi < 135.0) || (UnlimState.PortAzi < -45.0 && UnlimState.PortAzi > -135.0))
		UnlimState.PortThrust *= 0.1;

	// if else condition to reverse motors
	
	// Port
	if(UnlimState.PortAzi >= 135.0)
	{
		// reverse port direction
		UnlimState.PortThrust *= -1;
		UnlimState.PortAzi -= 180.0;
	}
	else if(UnlimState.PortAzi <= -135.0)
	{
		// reverse port direction
		UnlimState.PortThrust *= -1;
		UnlimState.PortAzi += 180.0;
	}

	
	// Stbd
	if(UnlimState.StbdAzi >= 137.5)
	{
		// reverse stbd direction
		UnlimState.StbdThrust *= -1;
		UnlimState.StbdAzi -= 180.0;
	}
	else if(UnlimState.StbdAzi <= -135.0)
	{
		// reverse stbd direction
		UnlimState.StbdThrust *= -1;
		UnlimState.StbdAzi += 180.0;
	}
	

	
	// apply saturation function
	LimState.PortThrust = Saturation(UnlimState.PortThrust, NEGMAX/2.0, TMAX/2.0);
	LimState.StbdThrust = Saturation(UnlimState.StbdThrust, NEGMAX/2.0, TMAX/2.0);
	LimState.PortAzi = Saturation(UnlimState.PortAzi, -45.0, 45.0);
	LimState.StbdAzi = Saturation(UnlimState.StbdAzi, -42.5, 45.0);
	
	return LimState;
}

char portAngle2Byte(float angle)
{/*
	This function calculates the byte sent to the Polulu MiniMaestro from an angle command. Assumes  a semi-linear relationship between thrust and motor command.
	
	Input, angle - angle between -45 and 45 deg
	
	Output, byte - 8-bit representation of angle command to be sent to polulu
*/
	char byte;
	
	// handle saturation 
	angle = Saturation(angle, -45.0, 45.0);
	
	byte = (char)(-2.55556*angle+118);
	return byte;
}

char stbdAngle2Byte(float angle)
{/*
	This function calculates the byte sent to the Polulu MiniMaestro from an angle command. Assumes  a semi-linear relationship between thrust and motor command.
	
	Input, angle - angle between -45 and 45 deg
	
	Output, byte - 8-bit representation of angle command to be sent to polulu
*/
	char byte;
	
	angle = Saturation(angle, -45.0, 45.0);

	if(angle < 25 && angle >= -45) byte = (char )(-2.75*angle+118);
	else if(angle >= 25 && angle <= 45) byte = (char )(-1.67*angle+90.985);

	return byte;
}

