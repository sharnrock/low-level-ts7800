/*
	Original Author: Ivan Bertaska
	Last Updated by: Ivan Bertaska
	Last Updated: 07/10/2014
	
	PD Controller acting on body fixed position error
	
	LCM Channels:
	IN:
		MAN2CONT
		STATE
	
	OUT:
		MOTORS
*/

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <math.h>
# include <termios.h>
# include <fcntl.h>
# include <sys/select.h>
# include <libconfig.h>
# include <lcm/lcm.h>
# include <time.h>

# define _BENCHTEST
# define _SUPERLOOP
# define _UI

# define PORT 0x02
# define STBD 0x01
# define PTAZI 0x05
# define SBAZI 0x04
# define PORTNEUTRAL 115
# define STBDNEUTRAL 118
# define RESETTIME 1.0

# define _CLEARSCREEN printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
// LCM Includes
# include "Man2Cont_Man2Cont_t.h"
# include "RXState_RXState_t.h"
# include "RXmotors_RXmotors_t.h"

// local includes
# include "global.h"
# include "RXControl.h"
# include "RXTrajTrack.h"
# include "RXStationKeeping.h"
# include "RXiir.h"
# include "RXDirectControl.h"

int main()
{
	// Configuration variables
	TRAJGAINstruct trajGains;
	SKGAINstruct skGains;
	LPFTIMEstruct lpfTime;
	
	double dbeta;
	float beta;
	long double timestamp;
	
	int tmp;
	
	// filename
	const char * filename = "../Config/RXContConfig.txt";
	
	// Common LCM Variables
	int Mmsg=0; // status bit
	int Smsg=0;
	struct timeval timeout = {0,5}; // time to wait for message = 5 milliseconds
	
	
	// Specific Startup Variables
	#ifndef _CONTTEST
	POSEstruct desired = {
		.xt = -100,
		.yt = -100,
		.ut = -1,
		.vt = -1,
		};
	
	NEDstruct curr_pose = {
		.north = -100,
		.east = -100,
		};
	
	BODstruct curr_vel = {
		.surge = -1,
		.sway = -1,
		};
		
	float yaw = -1;
	
	// trajectory tracking controller structures
	POSEstruct DesiredPOSE_AltStruct;
	TRAJCONTstruct TrajControlForces;
	TRAJCONTstruct TrajLimForces;
	THRUstruct TrajThrust;
	
	// station keeping controller structures
	float errorPsi;
	SKCONTstruct SKControlForces;
	SKMotorState MotorState;
	
	// low pass filters
	iirStruct iirPortThrust = iirFilterInit();
	iirStruct iirPortAzi = iirFilterInit();
	iirStruct iirStbdThrust = iirFilterInit();
	iirStruct iirStbdAzi = iirFilterInit();
	
	// timer for direct control mode reset
	long double start, curr;
	double diff;

	// common control variables
	POSEstruct bfe;
	char byteTp, byteTs, byteAzip, byteAzis;
	char LastMode = -1;
	#endif
	
	// input character
	int i;
	
	
	#ifdef _UI
	// UI variables
	char chan, val; 
	int ichan, ival;
	#endif
	
	/////////////////////
	//START-UP MESSAGES//
	/////////////////////
	
	printf("\n\n\tFLORIDA ATLANTIC UNVIERSTIY (c) 2014\n");
	printf("\tROBOTX CHALLENGE - SINGAPORE BAY - 2014\n\n\n");
	
	printf("\t ||_____|| \t ||_____|| \t ||_____|| \t ||_____||\n");
	printf("\t |[o] [o]| \t |[o] [o]| \t |[o] [o]| \t |[o] [o]|\n");
    printf("\t |   V   | \t |   V   | \t |   V   | \t |   V   |\n");
	printf("\t |       | \t |       | \t |       | \t |       |\n");
	printf("\t-ooo---ooo-\t-ooo---ooo-\t-ooo---ooo-\t-ooo---ooo-\n\n\n");
	printf("\tRXControl START UP\n");
	
	printf("\tTIME AT START (UTC): %lf\n", GetTimeStamp());
	#ifdef _DEBUG
	printf("\tDEBUG: ON\n\n\n");
	#endif 
	#ifndef _DEBUG
	printf("\tDEBUG: OFF\n\n\n");
	#endif
	
	usleep(500000);
	
	////////////////////////////
	//LOAD CONFIGURATIONS FILE//
	////////////////////////////
	
	printf("\tRXControl CONFIG FILE LOAD\n");
	// start by setting gain configs
	if(tmp = RXContFileRead(filename, &trajGains, &skGains, &lpfTime, &dbeta))
	{
		printf("\tRXControl CONFIG FILE: SUCCESSFUL\n\n");
		printf("\tTRAJECTORY GAINS\n");
		printf("\tKx\tKu\tKy\tKv\n");
		printf("\t%.3f\t%.3f\t%.3f\t%.3f\n\n", trajGains.Kx, trajGains.Ku, trajGains.Ky, trajGains.Kv);
		printf("\tSTATIONKEEPING GAINS\n");
		printf("\tKx\tKu\tKy\tKv\tKpsi\n");
		printf("\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n\n", skGains.Kx, skGains.Ku, skGains.Ky, skGains.Kv, skGains.Kpsi);
		printf("\tLPF GAINS\n");
		printf("\tTtp Tap Tsp Tap\n");
		printf("\t%.3f\t%.3f\t%.3f\t%.3f\n\n", lpfTime.ThrustTime, lpfTime.AziTime, lpfTime.ThrustTime, lpfTime.AziTime);
		printf("\tSAMPLE TIME\n");
		printf("\t%.3f\n", lpfTime.Ts);
		printf("\tEXPONENTIAL DECAY RATE\n");
		printf("\tbeta = %f\n\n\n\n", dbeta);
		
		usleep(500000);
		// cast beta to float from double
		beta = (float)dbeta;
	}
	else
	{
		printf("\tERROR IN CONFIG FILE - CHECK FOR FORMATTING ERRORS\n");
		return -1;
	}
		
		
	/////////////////////
	//OPEN LCM CHANNELS//
	/////////////////////
	
	printf("\tOPENING LCM CHANNELS\n");
	
	printf("\tOPENING LCM CHANNEL FROM MANEUVERING EXECUTABLE\n");
	
	Man2Cont_Man2Cont_t_subscription_t * LCMManeuveringPtr; // subscription pointer
	Man2Cont_Man2Cont_t DesiredPOSE; // structure to pass messages to
	
	// open from Maneuvering Program
	lcm_t * LCMManeuvering = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if( !LCMManeuvering)
	{
		printf("\tLCM FAILURE ON STARTUP TO: MANEUVERING\n");
		printf("\tPROGRAM CRASH IMMINENT\n\n\n");
        return 1;
	}
		
    LCMManeuveringPtr = Man2Cont_Man2Cont_t_subscribe(LCMManeuvering, "MAN2CONT", &Maneuvering_Handler, &DesiredPOSE);
	
	// set queue capacity to one, store only latest capacity
	Man2Cont_Man2Cont_t_subscription_set_queue_capacity(LCMManeuveringPtr, 1);
	
	printf("\tLCM CHANNEL OPEN 'MAN2CONT' : SUCCESSFUL\n\n\n");
	
	// State initialization
	
	RXState_RXState_t_subscription_t * LCMStatePtr; // subscription pointer
	RXState_RXState_t CurrentPOSE; // structure to pass messages to
	
	// open from State Program
	lcm_t * LCMState = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if( !LCMState)
	{
		printf("\tLCM FAILURE ON STARTUP TO: STATE\n");
		printf("\tPROGRAM CRASH IMMINENT\n\n\n");
        return 1;
	}
		
    LCMStatePtr = RXState_RXState_t_subscribe(LCMState, "STATE", &State_Handler, &CurrentPOSE);
	
	// set queue capacity to one, store only latest capacity
	RXState_RXState_t_subscription_set_queue_capacity(LCMStatePtr, 1);
	
	printf("\tLCM CHANNEL OPEN 'STATE' : SUCCESSFUL\n\n\n");
	
	// motor initialization
	RXmotors_RXmotors_t RXmotors_struct;
	
	lcm_t * LCMmotors = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if( !LCMmotors)
	{
		printf("\tLCM FAILURE ON STARTUP TO: MOTORS\n");
		printf("\tPROGRAM CRASH IMMINENT\n\n\n");
        return 1;
	}
	
	//////////////////////
	//OPEN POLULU SERIAL//
	//////////////////////
	
	printf("\tOPENING COMMUNICATION CHANNEL TO POLULU MINIMAESTRO\n");
	
	int MiniMaestroFD; // Polulu file descriptor
	struct termios newtio;       	
	const char *devicename = "/dev/ttts9";		
	
	printf("\tOPENING SERIAL PORT TO: %s\n", devicename);

	MiniMaestroFD = open(devicename , O_RDWR | O_NOCTTY | O_NONBLOCK);
	newtio.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = IGNPAR;
	newtio.c_oflag = 0;
	newtio.c_lflag = 0;
	newtio.c_cc[VMIN]=1;
	newtio.c_cc[VTIME]=0;
	tcflush(MiniMaestroFD, TCIOFLUSH); // flush data
	tcsetattr(MiniMaestroFD,TCSANOW,&newtio);
	
	printf("\tPOLULU MINIMAESTRO COMM: SUCCESSFUL\n\n\n");
	
	/////////////////////////////////////////////
	//CALCULATE IIR FILTER PARAMETERS FROM FILE//
	/////////////////////////////////////////////
	
	printf("\tCALCULATING DIGITAL FILTER COEFFICIENTS\n");
	
	// using bilinear transform
	
	// Port Thrust
	iirPortThrust.b[0] = 1.0/(1.0 + 2.0*lpfTime.ThrustTime/lpfTime.Ts);
	iirPortThrust.b[1] = 1.0/(1.0 + 2.0*lpfTime.ThrustTime/lpfTime.Ts);
	iirPortThrust.a = (1.0 - 2.0*lpfTime.ThrustTime/lpfTime.Ts)/(1.0 + 2.0*lpfTime.ThrustTime/lpfTime.Ts);
	
	// Stbd Thrust
	iirStbdThrust.b[0] = 1.0/(1.0 + 2.0*lpfTime.ThrustTime/lpfTime.Ts);
	iirStbdThrust.b[1] = 1.0/(1.0 + 2.0*lpfTime.ThrustTime/lpfTime.Ts);
	iirStbdThrust.a = (1.0 - 2.0*lpfTime.ThrustTime/lpfTime.Ts)/(1.0 + 2.0*lpfTime.ThrustTime/lpfTime.Ts);
	
	// Port Azimuth
	iirPortAzi.b[0] = 1.0/(1.0 + 2.0*lpfTime.AziTime/lpfTime.Ts);
	iirPortAzi.b[1] = 1.0/(1.0 + 2.0*lpfTime.AziTime/lpfTime.Ts);
	iirPortAzi.a = (1.0 - 2.0*lpfTime.AziTime/lpfTime.Ts)/(1.0 + 2.0*lpfTime.AziTime/lpfTime.Ts);
	
	// Stbd Azimuth
	iirStbdAzi.b[0] = 1.0/(1.0 + 2.0*lpfTime.AziTime/lpfTime.Ts);
	iirStbdAzi.b[1] = 1.0/(1.0 + 2.0*lpfTime.AziTime/lpfTime.Ts);
	iirStbdAzi.a = (1.0 - 2.0*lpfTime.AziTime/lpfTime.Ts)/(1.0 + 2.0*lpfTime.AziTime/lpfTime.Ts);
	
	#ifdef _DEBUG
	printf("\n\tIIR FILTER COEFFICIENTS\n\n");
	printf("\t\t   b0\t\tb1\t\ta0\n");
	printf("\tPORT THR: [%f\t%f\t%f]\n",iirPortThrust.b[0],iirPortThrust.b[1],iirPortThrust.a);
	printf("\tPORT AZI: [%f\t%f\t%f]\n",iirPortAzi.b[0],iirPortAzi.b[1],iirPortAzi.a);
	printf("\tSTBD THR: [%f\t%f\t%f]\n",iirStbdThrust.b[0],iirStbdThrust.b[1],iirStbdThrust.a);
	printf("\tSTBD AZI: [%f\t%f\t%f]\n\n",iirStbdAzi.b[0],iirStbdAzi.b[1],iirStbdAzi.a);
	
	usleep(500000);
	#endif
	
	// print dummy message
	
	RXmotors_struct.PortState[0] = 0;
	RXmotors_struct.PortState[1] = 0;
	RXmotors_struct.StbdState[0] = 0;
	RXmotors_struct.StbdState[1] = 0;
	
	RXmotors_RXmotors_t_publish(LCMmotors,"MOTORS",&RXmotors_struct);
	
	#ifdef _UI
	while(1)
	{
	
		//////////////////
		//USER INTERFACE//
		//////////////////
		printf("\tWELCOME TO ROBOTX LOWER-LEVEL CONTROLLER. HOW MAY I BE OF SERVICE?\n\n\n");
		printf("\t[1] Query controller gains\n");
		printf("\t[2] Test 'MAN2CONT' LCM Channel\n");
		printf("\t[3] Test 'STATE' LCM Channel\n");
		printf("\t[4] Manually enter values for motor\n");
		printf("\t[5] START\n");
		printf("\t[0] EXIT\n");
		printf("\n\n");

		printf(">> ");
		scanf("%d",&i);
		#endif
		
		#ifndef _UI
		i = 5; // default value to go to super loop
		#endif
		
		switch(i) {
				case 0:
				
				
				////////
				//EXIT//
				////////
				
				return 0;
				
				break;
				case 1:
				
				
				//////////////////////////
				//QUERY CONTROLLER GAINS//
				//////////////////////////
				printf("\tQUERY CONTROLLER GAINS\n\n");
				printf("\tTraj Kx   = %.3f\n",trajGains.Kx);
				printf("\tTraj Ky   = %.3f\n",trajGains.Ky);
				printf("\tTraj Ku   = %.3f\n",trajGains.Ku);
				printf("\tTraj Kv   = %.3f\n\n",trajGains.Kv);
				
				printf("\tSK   Kx   = %.3f\n",skGains.Kx);
				printf("\tSK   Ky   = %.3f\n",skGains.Ky);
				printf("\tSK   Ku   = %.3f\n",skGains.Ku);
				printf("\tSK   Kv   = %.3f\n",skGains.Kv);
				printf("\tSK   Kpsi = %.3f\n\n",skGains.Kpsi);
				
				printf("\tLPF  Thru = %.3f\n",lpfTime.ThrustTime);
				printf("\tLPF  Azi  = %.3f\n\n",lpfTime.AziTime);
				printf("\tLPF  Ts   = %.3f\n\n",lpfTime.Ts);
				
				printf("\tbeta = %.5f\n\n\n",beta);
				
				usleep(500000);
				
				break;
				case 2:
				
				
				/////////////////////////
				//TEST MAN2CONT CHANNEL//
				/////////////////////////
				
				lcm_handle(LCMManeuvering);
				
				printf("\tRECEIVED FROM 'MAN2CONT' CHANNEL: \n");
				printf("\tMODE   = %d\n\n",DesiredPOSE.mode);
				printf("\txt     = %f\n",DesiredPOSE.xt);
				printf("\tyt     = %f\n",DesiredPOSE.yt);
				printf("\tut     = %f\n",DesiredPOSE.ut);
				printf("\tvt     = %f\n\n",DesiredPOSE.vt);
				printf("\tSKx    = %f\n",DesiredPOSE.sk_x);
				printf("\tSKy    = %f\n",DesiredPOSE.sk_y);
				printf("\tSKpsi  = %f\n\n",DesiredPOSE.sk_psi);
				printf("\tPortCmd= %d\n",DesiredPOSE.PortCmd);
				printf("\tStbdCmd= %d\n\n\n",DesiredPOSE.StbdCmd);
				
				usleep(500000);
				
				break;
				case 3:
				
			
				//////////////////////
				//TEST STATE CHANNEL//
				//////////////////////
				
				lcm_handle(LCMState);
				
				printf("\tRECEIVED FROM 'STATE' CHANNEL: \n");
				printf("\tx    = %f\n",CurrentPOSE.USV_pose[0]);
				printf("\ty    = %f\n",CurrentPOSE.USV_pose[1]);
				printf("\tpsi  = %f\n",CurrentPOSE.USV_pose[2]);
				printf("\tu    = %f\n",CurrentPOSE.USV_vel[0]);
				printf("\tv    = %f\n",CurrentPOSE.USV_vel[1]);
				printf("\troll = %f\n",CurrentPOSE.USV_euler[0]);
				printf("\tpitch= %f\n",CurrentPOSE.USV_euler[1]);
				printf("\tyaw  = %f\n",CurrentPOSE.USV_euler[2]);
				printf("\tPort = [%f\t%f]\n",CurrentPOSE.PortState[0],CurrentPOSE.PortState[1]);
				printf("\tStbd = [%f\t%f]\n",CurrentPOSE.StbdState[0],CurrentPOSE.StbdState[1]);
				printf("\tt    = %f\n\n\n",CurrentPOSE.ElapsedTime);
				
				usleep(500000);
				
				break;
				case 4:
				
				
				printf("\tSET VALUE ON POLOLU MINIMAESTRO:\n\n\n");
				
				printf("\tSelect Channel value: \n");
				printf("\tChannel [1] = STBD PROPELLER\n");
				printf("\tChannel [2] = PORT PROPELLER\n");
				printf("\tChannel [?] = PORT LINEAR ACTUATOR\n");
				printf("\tChannel [?] = STBD LINEAR ACTUATOR\n\n\n");
				printf(">> ");
				scanf("%u",&ichan);
				chan = (char)ichan;
				
				printf("\n\n\tSelect value for channel (0-255): \n\n\n");
				printf(">> ");
				scanf("%u",&ival);
				val = (char)ival;
				
				TS78002Polulu(MiniMaestroFD, chan, val);
				usleep(500000);
				
				break;
				case 5:
				
				
				///////////////////
				//SUPERLOOP START//
				///////////////////
				
				#ifdef _SUPERLOOP
				printf("\tCONTROLLER START\n\n");
				while(1)
				{
					// Check for message from MAN2CONT channel
					Mmsg = LCMCheckForMessage(LCMManeuvering, &timeout);
					
					if(Mmsg)
					{
						// set previus mode to current mode before update
						LastMode = DesiredPOSE.mode;
						
						// handle message
						lcm_handle(LCMManeuvering);
						
						#ifdef _DEBUG
						printf("\tRECEIVED FROM 'MAN2CONT' CHANNEL: \n");
						printf("\tMODE   = %d\n\n",DesiredPOSE.mode);
						printf("\txt     = %f\n",DesiredPOSE.xt);
						printf("\tyt     = %f\n",DesiredPOSE.yt);
						printf("\tut     = %f\n",DesiredPOSE.ut);
						printf("\tvt     = %f\n\n",DesiredPOSE.vt);
						printf("\tSKx    = %f\n",DesiredPOSE.sk_x);
						printf("\tSKy    = %f\n",DesiredPOSE.sk_y);
						printf("\tSKpsi  = %f\n\n",DesiredPOSE.sk_psi);
						printf("\tPortCmd= %d\n",DesiredPOSE.PortCmd);
						printf("\tStbdCmd= %d\n\n\n",DesiredPOSE.StbdCmd);
						#endif
						
						if(DesiredPOSE.mode == 0)
						{
							///////////////////////
							//TRAJECTORY TRACKING//
							///////////////////////
							
							// store values from message in alternate structure of type POSEstruct
							DesiredPOSE_AltStruct.xt = DesiredPOSE.xt;
							DesiredPOSE_AltStruct.yt = DesiredPOSE.yt;
							DesiredPOSE_AltStruct.ut = DesiredPOSE.ut;
							DesiredPOSE_AltStruct.vt = DesiredPOSE.vt;
						}
						else if(DesiredPOSE.mode == 1)
						{
							///////////////////
							//STATION KEEPING//
							///////////////////
							
							// store values from message in alternate structure of type POSEstruct
							DesiredPOSE_AltStruct.xt = DesiredPOSE.sk_x;
							DesiredPOSE_AltStruct.yt = DesiredPOSE.sk_y;
							DesiredPOSE_AltStruct.ut = 0.0;
							DesiredPOSE_AltStruct.vt = 0.0;
						}
						
						// check for change in mode
						if(DesiredPOSE.mode!=LastMode && DesiredPOSE.mode==0)
						{
							// place azimuthing thrusters at neutral location
							TS78002Polulu(MiniMaestroFD, PTAZI, PORTNEUTRAL);
							TS78002Polulu(MiniMaestroFD, SBAZI, STBDNEUTRAL);
						
						}
						else if(DesiredPOSE.mode!=LastMode && DesiredPOSE.mode==1)
						{
							// if new, clear buffers
							iirFlushBuffer(&iirPortThrust);
							iirFlushBuffer(&iirStbdThrust);
							iirFlushBuffer(&iirPortAzi);
							iirFlushBuffer(&iirStbdAzi);
						}
						else if(DesiredPOSE.mode!=LastMode && DesiredPOSE.mode==2)
						{
							// start timer
							start = GetTimeStamp();
							
							// set neutral commands
							TS78002Polulu(MiniMaestroFD, PORT, 127);
							TS78002Polulu(MiniMaestroFD, STBD, 127);
							TS78002Polulu(MiniMaestroFD, PTAZI, PORTNEUTRAL);
							TS78002Polulu(MiniMaestroFD, SBAZI, STBDNEUTRAL);
							
							// update commands in MOTORS LCM channel
							RXmotors_struct.PortState[0] = 0;
							RXmotors_struct.PortState[1] = 0;
							RXmotors_struct.StbdState[0] = 0;
							RXmotors_struct.StbdState[1] = 0;
							
							RXmotors_RXmotors_t_publish(LCMmotors,"MOTORS",&RXmotors_struct);
						}
						
					}
					
					// Check for message from State Channel
					
					Smsg = LCMCheckForMessage(LCMState, &timeout);
					if(Smsg)
					{
						lcm_handle(LCMState);
						#ifdef _DEBUG
						printf("\tRECEIVED FROM 'STATE' CHANNEL: \n");
						printf("\tx    = %f\n",CurrentPOSE.USV_pose[0]);
						printf("\ty    = %f\n",CurrentPOSE.USV_pose[1]);
						printf("\tpsi  = %f\n",CurrentPOSE.USV_pose[2]);
						printf("\tu    = %f\n",CurrentPOSE.USV_vel[0]);
						printf("\tv    = %f\n",CurrentPOSE.USV_vel[1]);
						printf("\troll = %f\n",CurrentPOSE.USV_euler[0]);
						printf("\tpitch= %f\n",CurrentPOSE.USV_euler[1]);
						printf("\tyaw  = %f\n",CurrentPOSE.USV_euler[2]);
						printf("\tPort = [%f\t%f]",CurrentPOSE.PortState[0],CurrentPOSE.PortState[1]);
						printf("\tStbd = [%f\t%f]",CurrentPOSE.StbdState[0],CurrentPOSE.StbdState[1]);
						printf("\tt    = %f\n\n\n",CurrentPOSE.ElapsedTime);
						#endif
						
						// NED Positions
						curr_pose.north = CurrentPOSE.USV_pose[0];
						curr_pose.east = CurrentPOSE.USV_pose[1];
						
						// orientation
						yaw = CurrentPOSE.USV_pose[2]*pi/180.0;
						
						// velocity
						curr_vel.surge = CurrentPOSE.USV_vel[0];
						curr_vel.sway = CurrentPOSE.USV_vel[1];
						
					}
					
					// tie updates to state channel
					if(Smsg)
					{
						switch(DesiredPOSE.mode) {
						
							case 0:
							
							///////////////////////
							//TRAJECTORY TRACKING//
							///////////////////////
							
							// calculate through all functions
							bfe = BodyFixedError(DesiredPOSE_AltStruct, curr_pose, curr_vel, yaw);
							TrajControlForces = PDTrajTrackCont(bfe, trajGains);
							TrajLimForces = TrajControlLimit(TrajControlForces, beta);
							TrajThrust = TrajControlForces2Thrust(TrajLimForces);
							
							// convert to byte to send to Polulu
							byteTp = Thrust2Byte(TrajThrust.Tp);
							byteTs = Thrust2Byte(TrajThrust.Ts);
							
							// write to serial
							TS78002Polulu(MiniMaestroFD, PORT, byteTp);
							TS78002Polulu(MiniMaestroFD, STBD, byteTs);
							
							// reset azimuthing thrusters
							TS78002Polulu(MiniMaestroFD, PTAZI, PORTNEUTRAL);
							TS78002Polulu(MiniMaestroFD, SBAZI, STBDNEUTRAL);
							
							// update commands in MOTORS LCM channel
							RXmotors_struct.PortState[0] = Byte2MotorPower(byteTp);
							RXmotors_struct.PortState[1] = 0;
							RXmotors_struct.StbdState[0] = Byte2MotorPower(byteTs);
							RXmotors_struct.StbdState[1] = 0;
							
							RXmotors_RXmotors_t_publish(LCMmotors,"MOTORS",&RXmotors_struct);
						
							break;
							
							case 1:
							
							///////////////////
							//STATION KEEPING//
							///////////////////
							
							bfe = BodyFixedError(DesiredPOSE_AltStruct, curr_pose, curr_vel, yaw);
							errorPsi = HeadingErrorWrap(DesiredPOSE.sk_psi*pi/180.0, yaw); // in radians
							//SKControlForces = StationKeepingController(bfe, skGains, errorPsi); 
							SKControlForces = StationKeepingRobustController(skGains, DesiredPOSE_AltStruct, curr_pose, curr_vel, CurrentPOSE.USV_pose[2]*pi/180.0, DesiredPOSE.sk_psi*pi/180.0, CurrentPOSE.USV_pose[2]);    //EDO's
							MotorState = SKControlAllocation(SKControlForces); // in degrees
							
							// store commands in proper location
							iirPortThrust.x[0] = MotorState.PortThrust;
							iirStbdThrust.x[0] = MotorState.StbdThrust;
							iirPortAzi.x[0] = MotorState.PortAzi; // degrees
							iirStbdAzi.x[0] = MotorState.StbdAzi; // degrees
							
							// perform low pass filters on commands
							iirFilter(&iirPortThrust);
							iirFilter(&iirStbdThrust);
							iirFilter(&iirPortAzi);
							iirFilter(&iirStbdAzi);
							
							// move output from filter to MotorState structure
							MotorState.PortThrust = iirPortThrust.y[0];
							MotorState.StbdThrust = iirStbdThrust.y[0];
							MotorState.PortAzi = iirPortAzi.y[0];
							MotorState.StbdAzi = iirStbdAzi.y[0];
							
							// convert to byte to send to Polulu
							byteTp = Thrust2Byte(MotorState.PortThrust);
							byteTs = Thrust2Byte(MotorState.StbdThrust);
							byteAzip = portAngle2Byte(MotorState.PortAzi);
							byteAzis = stbdAngle2Byte(MotorState.StbdAzi);
							
							TS78002Polulu(MiniMaestroFD, PORT, byteTp);
							TS78002Polulu(MiniMaestroFD, STBD, byteTs);
							TS78002Polulu(MiniMaestroFD, PTAZI, byteAzip);
							TS78002Polulu(MiniMaestroFD, SBAZI, byteAzis);
							
							// update commands in MOTORS LCM channel
							RXmotors_struct.PortState[0] = Byte2MotorPower(byteTp);
							RXmotors_struct.PortState[1] = Byte2AnglePercent(byteAzip);
							RXmotors_struct.StbdState[0] = Byte2MotorPower(byteTs);
							RXmotors_struct.StbdState[1] = Byte2AnglePercent(byteAzis);
							
							RXmotors_RXmotors_t_publish(LCMmotors,"MOTORS",&RXmotors_struct);
							
						
							break;
							
							case 2:
							
							// get timestamp 
							curr = GetTimeStamp();
							diff = (double)(curr-start);
							
							if(diff > RESETTIME)
							{
								// if difference is greater than time it takes to reset
								// update motors with desired command
								
								byteTp = MotorPower2Byte(DesiredPOSE.PortCmd);
								byteTs = MotorPower2Byte(DesiredPOSE.StbdCmd);
								
								// reset azimuth to neutral
								TS78002Polulu(MiniMaestroFD, PTAZI, PORTNEUTRAL);
								TS78002Polulu(MiniMaestroFD, SBAZI, STBDNEUTRAL);
								
								// write command to port and stbd motors
								TS78002Polulu(MiniMaestroFD, PORT, byteTp);
								TS78002Polulu(MiniMaestroFD, STBD, byteTs);
								
								// update commands in MOTORS LCM channel
								RXmotors_struct.PortState[0] = Byte2MotorPower(byteTp);
								RXmotors_struct.PortState[1] = 0;
								RXmotors_struct.StbdState[0] = Byte2MotorPower(byteTs);
								RXmotors_struct.StbdState[1] = 0;
								
								RXmotors_RXmotors_t_publish(LCMmotors,"MOTORS",&RXmotors_struct);
							}
							
							
							break;
							
							default:
							printf("\tCONTROL MODE NOT WITHIN RANGE. STOPPING MOTORS!!\n\n");
							// set neutral commands
							TS78002Polulu(MiniMaestroFD, PORT, 127);
							TS78002Polulu(MiniMaestroFD, STBD, 127);
							TS78002Polulu(MiniMaestroFD, PTAZI, PORTNEUTRAL);
							TS78002Polulu(MiniMaestroFD, SBAZI, STBDNEUTRAL);
							
							// update commands in MOTORS LCM channel
							RXmotors_struct.PortState[0] = 0;
							RXmotors_struct.PortState[1] = 0;
							RXmotors_struct.StbdState[0] = 0;
							RXmotors_struct.StbdState[1] = 0;
							
							RXmotors_RXmotors_t_publish(LCMmotors,"MOTORS",&RXmotors_struct);
						}
						
						// reset flags
						Mmsg = 0; 
						Smsg = 0;
						
						#ifdef _BENCHTEST
						printf("\tCONTROL MODE: %d\n", DesiredPOSE.mode);
						
						if(DesiredPOSE.mode == 0)
						{
							printf("\tDESIRED POSE\n");
							printf("\t[%.3f\t%.3f\t%.3f\t%.3f]\n\n",DesiredPOSE_AltStruct.xt, DesiredPOSE_AltStruct.yt, curr_vel.surge+bfe.ut, curr_vel.sway+bfe.vt);
							printf("\tACTUAL POSE\n");
							printf("\t[%.3f\t%.3f\t%.3f\t%.3f]\n\n", curr_pose.north, curr_pose.east, curr_vel.surge, curr_vel.sway);
							printf("\tSATURATED THRUST\n");
							printf("\t[%.3f\t%.3f]\n\n", TrajLimForces.X, TrajLimForces.N);
							printf("\tTHRUST FOR MOTORS\n");
							printf("\t[%.3f\t%.3f]\n\n",TrajThrust.Tp, TrajThrust.Ts);
							printf("\tBYTES SENT TO MXU\n");
							printf("\t[%u\t%u]\n\n\n",byteTp, byteTs);
						}
						else if(DesiredPOSE.mode == 1)
						{
							printf("\tDESIRED POSE\n");
							printf("\t[%.3f\t%.3f\t%.3f\t%.3f\t%.3f]\n\n",DesiredPOSE_AltStruct.xt, DesiredPOSE_AltStruct.yt, curr_vel.surge+bfe.ut, curr_vel.sway+bfe.vt,DesiredPOSE.sk_psi);
							printf("\tACTUAL POSE\n");
							printf("\t[%.3f\t%.3f\t%.3f\t%.3f\t%.3f]\n\n", curr_pose.north, curr_pose.east, curr_vel.surge, curr_vel.sway, CurrentPOSE.USV_pose[2]);
							printf("\tMOTOR STATE\n");
							printf("\t[%.3f\t%.3f\t%.3f\t%.3f]\n\n",MotorState.PortThrust, MotorState.StbdThrust, MotorState.PortAzi, MotorState.StbdAzi);
							printf("\tBYTES SENT TO MXU\n");
							printf("\t[%u\t%u\t%u\t%u]\n\n",byteTp, byteTs, byteAzip, byteAzis);
							printf("\tHEADING ERROR \n");
							printf("\t[%f]\t\n\n",errorPsi);
							printf("\tDESIRED FORCES/TORQUES\n");
							printf("\t[%f\t%f\t%f]\n\n", SKControlForces.X, SKControlForces.Y, SKControlForces.N);
							printf("\tBF ERRORS\n");
							printf("\t[%f\t%f\t%f]\n\n\n",bfe.xt, bfe.yt, errorPsi);
						}
						else if(DesiredPOSE.mode == 2)
						{
							printf("\tMOTOR COMMANDS IN POWER\n");
							printf("\t[%d\t%d]\n\n",DesiredPOSE.PortCmd, DesiredPOSE.StbdCmd);
							printf("\tMOTOR COMMANDS BYTES\n");
							printf("\t[%u\t%u]\n\n\n",byteTp,byteTs);
							
						}

						#endif
					}
				}
				#endif
				
				break;
		}
		
	#ifdef _UI
	} // end of USER INTERFACE while loop
	#endif
}

