# define TMAX 240.0 // USV16 Parameters
# define NEGMAX -102.0
# define NMAX 171.0
  
  
  ///////////////
  //STRUCTURES//
  //////////////
  
  // structures ...

typedef struct {
	float Kx;
	float Ku;
	float Ky;
	float Kv;
} TRAJGAINstruct;

typedef struct {
	float Kx;
	float Ku;
	float Ky;
	float Kv;
	float Kpsi;
} SKGAINstruct;

typedef struct {
	float ThrustTime;
	float AziTime;
	float Ts;
} LPFTIMEstruct;

typedef struct {
	float X;
	float N;
} TRAJCONTstruct;

typedef struct {
	float X;
	float Y;
	float N;
} SKCONTstruct;

typedef struct {
	float PortThrust;
	float PortAzi;
	float StbdThrust;
	float StbdAzi;
} SKMotorState;

typedef struct {
	float Tp;
	float Ts;
} THRUstruct;
  
  ///////////////////
  // CONFIGURATION //
  ///////////////////

int RXContFileRead(const char * config_file_name, TRAJGAINstruct * trajGains, SKGAINstruct * skGains, LPFTIMEstruct * lpfTime, double * beta)
{
    config_t cfg;               /*Returns all parameters in this structure */
    config_setting_t *setting;
    const char *str1; // filename string- local variable
    int tmp;
	double ftmp;
 
    // Initialization
    config_init(&cfg);
	
    // Read the file. If there is an error, report it and exit.
    if (!config_read_file(&cfg, config_file_name))
    {
        printf("\n\n\tRX CONFIG FILE READ PROBLEM.\n\n");
        config_destroy(&cfg);
        return 0;
    }
 
    ////////////////
	//NAME OF FILE//
	////////////////
	
    if (config_lookup_string(&cfg, "filename", &str1))
        printf("\n\tFile Type: %s", str1);
    else
        printf("\n\tNo 'filename' setting in configuration file.");
		
	////////////////////
	//TRAJ TRACK GAINS//
	////////////////////
	
	if (config_lookup_float(&cfg, "trajKx", &ftmp))
	{
		trajGains->Kx = (float)ftmp;
        printf("\n\tParam value: %f", trajGains->Kx);
	}
    else
        printf("\n\tNo 'trajKx' setting in configuration file.");
		
	if (config_lookup_float(&cfg, "trajKu", &ftmp))
	{
		trajGains->Ku = (float)ftmp;
        printf("\n\tParam value: %f", trajGains->Ku);
	}
    else
        printf("\n\tNo 'trajKu' setting in configuration file.");
		
	if (config_lookup_float(&cfg, "trajKy", &ftmp))
	{
		trajGains->Ky = (float)ftmp;
        printf("\n\tParam value: %f", trajGains->Ky);
	}
    else
        printf("\n\tNo 'trajKy' setting in configuration file.");
		
	if (config_lookup_float(&cfg, "trajKv", &ftmp))
	{
		trajGains->Kv = (float)ftmp;
        printf("\n\tParam value: %f", trajGains->Kv);
	}
    else
        printf("\n\tNo 'trajKv' setting in configuration file.");
		
	////////////////////////
	//STATIONKEEPING GAINS//
	////////////////////////
	
	if (config_lookup_float(&cfg, "skKx", &ftmp))
	{
		skGains->Kx = (float)ftmp;
        printf("\n\tParam value: %f", skGains->Kx);
	}
    else
        printf("\n\tNo 'skKx' setting in configuration file.");
		
	if (config_lookup_float(&cfg, "skKu", &ftmp))
	{
		skGains->Ku = (float)ftmp;
        printf("\n\tParam value: %f", skGains->Ku);
	}
    else
        printf("\n\tNo 'skKu' setting in configuration file.");
		
	if (config_lookup_float(&cfg, "skKy", &ftmp))
	{
		skGains->Ky = (float)ftmp;
        printf("\n\tParam value: %f", skGains->Ky);
	}
    else
        printf("\n\tNo 'skKy' setting in configuration file.");
		
	if (config_lookup_float(&cfg, "skKv", &ftmp))
	{
		skGains->Kv = (float)ftmp;
        printf("\n\tParam value: %f", skGains->Kv);
	}
    else
        printf("\n\tNo 'skKy' setting in configuration file.");
		
	if (config_lookup_float(&cfg, "skKpsi", &ftmp))
	{
		skGains->Kpsi = (float)ftmp;
        printf("\n\tParam value: %f", skGains->Kpsi);
	}
    else
        printf("\n\tNo 'skKpsi' setting in configuration file.");
		
	//////////////////////
	//LPF TIME CONSTANTS//
	//////////////////////
	
	if (config_lookup_float(&cfg, "lpfThrust", &ftmp))
	{
		lpfTime->ThrustTime = (float)ftmp;
        printf("\n\tParam value: %f", lpfTime->ThrustTime);
	}
    else
        printf("\n\tNo 'lpfThrust' setting in configuration file.");
		
	if (config_lookup_float(&cfg, "lpfAzi", &ftmp))
	{
		lpfTime->AziTime = (float)ftmp;
        printf("\n\tParam value: %f", lpfTime->AziTime);
	}
    else
        printf("\n\tNo 'lpfAzi' setting in configuration file.");
		
	if (config_lookup_float(&cfg, "Ts", &ftmp))
	{
		lpfTime->Ts = (float)ftmp;
        printf("\n\tParam value: %f", lpfTime->Ts);
	}
    else
        printf("\n\tNo 'Ts' setting in configuration file.");
	
	/////////////////////
	//EXPONENTIAL DECAY//
	/////////////////////
	
	if (config_lookup_float(&cfg, "beta", beta))
	{
        printf("\n\tParam value: %lf\n\n", beta);
	}
    else
        printf("\n\tNo 'beta' setting in configuration file.");
 
    config_destroy(&cfg);
	
	return 1; // return 1 on success
}

	///////////////////////
	//COMMON CONTROL FCNS//
	///////////////////////
	
POSEstruct BodyFixedError(POSEstruct desi_struct, NEDstruct curr_pose, BODstruct curr_velocity, float yaw)
{
/*
	This function calculates the body-fixed errors associated with surge/sway speed and surge/sway position.
	
	Input, desi_struct - desired velocities and position wrt to geo-fixed frame
	Input, curr_pose - current position in north and east (geographic)
	Input, curr_velocity - current velocity in surge and sway (body-fixed)
	Input, yaw - current vehicle headin wrt to North in radians

	Output, BFE_struct - body-fixed velocities and position error wrt to body-fixed frame
*/
	//Declare variables
	NEDstruct NED_pose_error, NED_desi_velocity; 
	BODstruct BOD_pose_error, BOD_desi_velocity;
	
	POSEstruct BFE_struct; // output variable
	
	
	// set up variables
	NED_pose_error.north = desi_struct.xt - curr_pose.north;
	NED_pose_error.east = desi_struct.yt - curr_pose.east;
	
	NED_desi_velocity.north = desi_struct.ut;
	NED_desi_velocity.east = desi_struct.vt;
	
	// perform transformations
	BOD_pose_error = Geo2Bod(NED_pose_error, yaw);
	BOD_desi_velocity = Geo2Bod(NED_desi_velocity, yaw);
	
	// fill in returned structure
	BFE_struct.xt = BOD_pose_error.surge;
	BFE_struct.yt = BOD_pose_error.sway;
	
	// subtract out current velocities to obtain error
	BFE_struct.ut = BOD_desi_velocity.surge - curr_velocity.surge;
	BFE_struct.vt = BOD_desi_velocity.sway - curr_velocity.sway;
	
	return BFE_struct;
}
	
char Thrust2Byte(float thrust)
{
/*
	This function calculates the byte sent to the Polulu MiniMaestro from a Thrust command. Assumes linear relationship between thrust and motor command.
	
	Input, thrust - thrust command, can be negative
	
	Output, byte - 8-bit representation of thrust command to be sent to polulu
*/	
	char byte;
	float y;

	// start piecewise linear curve fit from bollard pole tests
	if(thrust >= -51.0 && thrust < -6.65)
	{
		y = 1.0939*thrust-43.8594;
	}
	else if(thrust >= -6.65 && thrust <= 3.6)
	{
		y = 38.5/5.0*thrust;
	}
	else if(thrust > 3.6 && thrust <= 120.0)
	{
		y = 0.62*thrust+24.7310;
	}
	else
	{
		y = 0;
	}

	byte = (char) (y/100.0*127.0+127.0); ;
	
	return byte;
}

int Byte2MotorPower(char byte)
{/*
	This function converts a byte to a motor power percentage between -100 and 100
	
	Input, byte  - unsigned character
	
	Output, MotorPower - integer between -100 and 100
*/

	int MotorPower;
	
	MotorPower = (int)(100.0/127.0*(byte - 127.0));
	
	return MotorPower;
}

int Byte2AnglePercent(char byte)
{/*
	This function converts a byte to an angle percentage between -100 and 100
	
	Input, byte  - unsigned character
	
	Output, angle - integer between -100 and 100
*/

	int angle;
	
	// fill in function until neutral position is found
	angle = (int)(100.0/127.0*(byte - 127.0));
	
	return angle;
}

	/////////////////
	//COMMUNICATION//
	/////////////////

void TS78002Polulu(int comfd, char channel, char byte)
{/*
	This function writes a byte to a serial port comfd as started 
	
	Input, comfd - file descriptor from com channel
	Input, channel - channel on Polulu to write byte to
	Input, byte - byte to write to the channel
	
*/	
	
	// string with preamble to write to Polulu
	char letter[4] = {0xAA, 0xFF, 0x00, 0x00};
	
	// char saturation
	if (channel < 0) channel = 0;
	if (channel > 12) channel = 12;
	
	if (byte < 0) byte = 0;
	if (byte > 254) byte = 254;
	
	// set value on letter
	letter[2] = channel;
	letter[3] = byte;
	write(comfd, &letter, 4);
	
	#ifdef _DEBUG
	printf("\tWriting to Polulu = {%u %u %u %u}\n", letter[0], letter[1], letter[2], letter[3]);
	#endif 
}

  /////////////////
  //LCM FUNCTIONS//
  /////////////////
  
static void Maneuvering_Handler(const lcm_recv_buf_t *rbuf, const char * channel, const Man2Cont_Man2Cont_t * msg, void * user_data)
{
/*
	This function is used as a handler for the 'MAN2CONT'channel on LCM.
	
	Traditional LCM Inputs:
	
	Input, rbuf - LCM receive buffer
	Input, channel - channel name
	Input, msg - msg received
	Input, user_data - should be same type as msg, transfer all data from this handler to function
*/

	// copy to memory
	memcpy(user_data,msg,sizeof(Man2Cont_Man2Cont_t));
}

static void State_Handler(const lcm_recv_buf_t *rbuf, const char * channel, const RXState_RXState_t * msg, void * user_data)
{
/*
	This function is used as a handler for the 'MAN2CONT'channel on LCM.
	
	Traditional LCM Inputs:
	
	Input, rbuf - LCM receive buffer
	Input, channel - channel name
	Input, msg - msg received
	Input, user_data - should be same type as msg, transfer all data from this handler to function
*/

	// copy to memory
	memcpy(user_data,msg,sizeof(RXState_RXState_t));
}
