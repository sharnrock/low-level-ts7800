#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/select.h>

#include "compass_compass_t.h"

///////////////////////
typedef struct{
		float heading;
		float pitch;
		float roll;
		float temp;
		} compass_struct;


compass_struct parsing_compass(char string[50]);
int checksumcheck(char * NMEAstring);

//////////////////////////////////////////////////////////////////////
int main()
{  

	//Initialize LCM
    lcm_t * lcm = lcm_create("udpm://239.255.76.67:7667?ttl=1");
    if(!lcm)
        return 1;
		
	//compass structure
	compass_struct parse_struct;
	
	//create compass LCM structure
	compass_compass_t compass_lcm_struct;

	int check_flag = 0;
	//Initialize Serial Port
	    char *OSC_FILE_DES = "/dev/ttts1";//<---- The tttsn value of your serial port may change depending on which com port the compass is connected to
		char *OSC_BAUD_RATE = "B19200"; //<----default baudrate of 19200
		
		//File descriptors.
		int cmp_fd;
        struct termios old_cmp_tio, new_cmp_tio;
        int need_exit = 0;
        int cmp_ind = 0;
		
		//incoming data variables
	   char data_str[100];
	   
        //Initializing the COM port
        //Make the FD.
        cmp_fd = open(OSC_FILE_DES, O_RDWR | O_NOCTTY | O_NONBLOCK);

        //Check to see that "open" call was successful.
        if (cmp_fd < 0) { //If it broke....
                perror("open");
                return (EXIT_FAILURE);
        }
        //Print saying that was the case.
        printf("Opened COM port to OS5000:%s\r\n", OSC_FILE_DES);

        //Get current attributes for cmp_fd.
        if (tcgetattr(cmp_fd, &old_cmp_tio) < 0) { //If it broke...
                perror("tcgetattr -> cmp_fd:");
                return (EXIT_FAILURE);
        }
        
        
        //Make a new termios structure with the settings we want.
        new_cmp_tio.c_cflag = B19200 | CS8 | CLOCAL | CREAD;
        new_cmp_tio.c_iflag = IGNPAR;
        new_cmp_tio.c_oflag = 0;
        new_cmp_tio.c_lflag = 0;
        new_cmp_tio.c_cc[VMIN]=1;
        new_cmp_tio.c_cc[VTIME]=0;
        
        //Flush whatever is in the buffer for that file.
        if (tcflush(cmp_fd, TCIFLUSH) < 0) { //If it broke...
                perror("tcflush -> cmp_fd:");
                return (EXIT_FAILURE);
        }
        //Set new attributes.
        if (tcsetattr(cmp_fd, TCSANOW, &new_cmp_tio) < 0) { //If it broke...
                perror("tcsetattr -> cmp_fd:");
                return (EXIT_FAILURE);
        }

        do {
		
			   	//declare timeout structure if no new data in buffer
				//must be redeclared in loop to avoid select erroR
				struct timeval timeout = {1,0};
				char c;
				fd_set fds;
				int ret1, ret2;
			
                //Zero file descriptor set, then append all types.
                FD_ZERO(&fds);
                FD_SET(cmp_fd, &fds);
                FD_SET(STDIN_FILENO, &fds);
                ret1 = select(FD_SETSIZE, &fds, NULL, NULL, &timeout);
                if (ret1 == -1) 
				{ //If select returns an error.
                        perror("select");
                        need_exit = 1;
                } 
				else if (ret1 > 0) 
				{
                        if (FD_ISSET(cmp_fd, &fds)) 
						{
                                do 
								{ //Read while we get errors that are due to signals.
                                        ret2 = read(cmp_fd, &c, 1);
                                } while (ret2 < 0 && errno == EINTR);
                                
								if (ret2 == 1) {
                                        data_str[cmp_ind] = c;
                                        cmp_ind++; 
                                        if(c == '\n'){
                                                cmp_ind = 0;
												check_flag = checksumcheck(data_str);
												if(check_flag == 1)
												{
													parse_struct =  parsing_compass(data_str);
													//copy all files to lcm structure
													compass_lcm_struct.heading = parse_struct.heading;
													
													//printf("%f\n",variableheading);
													compass_compass_t_publish(lcm, "COMPASS", &compass_lcm_struct);
													check_flag = 0;
												}

                                        }
                                } else if(ret2 == -1) { //If we're at the start of a new string.
                                        perror("read");
                                        need_exit = 1;
                                }
                        }
                      
                } else if (ret1 == 0){
                        printf("select: timeout expired.\r\n");
                }
        } while (1);

        return 01;
	}
	
	
	



compass_struct parsing_compass(char string[50])
 {
 //this parses the standard NMEA string coming in from the OS5000 compass
 compass_struct return_struct;
 
 char letterdol;
 char letterC;
 float Heading; 
 char letterP;
 float Pitch;
 char letterR;
 float Roll;
 char letterT;
 float Temperature;
 char letteraster;
 int checksum;
 int i;
 
 //use sscanf to parse compass ASCII message
 i = sscanf(string, "%c %c  %f   %c   %f  %c  %f     %c   %f  %c   %d", &letterdol, &letterC, &Heading, &letterP, &Pitch, &letterR, &Roll, &letterT, &Temperature, &letteraster, &checksum);

 return_struct.heading = Heading;
 return_struct.pitch = Pitch;
 return_struct.roll = Roll;
 return_struct.temp = Temperature;
 
 
 return return_struct;
 }
 
 int checksumcheck(char * NMEAstring)
{
	//returns 1 if true, 0 if false
	char * starpose;
	int i = 1;
	char checksumchar[2];
	int  checksum_fromstring;
	int checksum_fromcalc = 0;
	
	if(NMEAstring[0] != '$')
	{
	//break out of function
	return 0;
	}
	starpose = strchr(NMEAstring, '*');
	if(starpose == NULL)
	return 0;
	
	strncpy(checksumchar, NMEAstring+(starpose-NMEAstring)+1, 2);
	checksumchar[2] = '\0';
	
	sscanf(checksumchar,"%x",&checksum_fromstring);
	for(i=1; i<starpose-NMEAstring; i++)
		checksum_fromcalc ^= NMEAstring[i];
		
	if(checksum_fromstring == checksum_fromcalc)
	return 1;
	
	else
	return 0;
}
