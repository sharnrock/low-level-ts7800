/*
	Original Author: Ivan Bertaska
	Last Updated by: Ivan Bertaska
	Last Updated: 11/07/2013
	
	This version of the IMU driver takes in information from the compass and uses that as
	the basis for the transformation matrix.
	
	IN:
		COMPASS
	
	OUT:
		IMU
		GPS
*/
//
#include <termios.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/select.h>

#include "imu_imu_t.h"
#include "gps_gps_t.h"
#include "compass_compass_t.h"

//global variable
float veh_heading;

typedef union float_making{
	float f;
	unsigned char word[4];
	}number;

struct timeval timeout = {
		0,
		5};
	
imu_imu_t imu_parse(char string_imu[200], int found_index_imu);

static void compass_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const compass_compass_t * msg, void * user)
{

	extern float veh_heading; 
	veh_heading = msg->heading;
}

int main()
{
	////////////////////
	//Global Variables//
	////////////////////
	
	extern float veh_heading;
	veh_heading = 0;
	
	int IMU_Port;
	struct termios IMU_oldtio, IMU_newtio;  //place for old and new port settings for serial port
	struct termios oldkey, newkey;       	//place tor old and new port settings for keyboard teletype
	int internal_status = 0;		
	
	/////////////////////////////////////////////////////////////
	//selecting the port that the IMU will be connected        //
	/////////////////////////////////////////////////////////////
	
	
	//char *devicename_imu = "/dev/ttts4";			//this is for the DUCKWBOARD
	
	char *devicename_imu = "/dev/ttts0";		//this is for the WAMVBOARD
	
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	
	char c;
	char string_imu[300];
	int need_exit = 0;
	int index_imu = 3;
	int found_index_imu;
	int state_IMU = 0;
	int checksum_IMU = 0x02;
	
	// List of configuration constants

	char gotoconfiguration[5] =	{0xFA, 0XFF, 0X30, 0X00, 0XD1}; // goes to configuration state
	//char caliorient[7] = 		{0xFA, 0XFF, 0XD0, 0X02, 0X00, 0X06, 0X29};// This command set the IMU to calibrated and orientation mode
	char caliorient[7] = 		{0xFA, 0XFF, 0XD0, 0X02, 0X00, 0X36, 0XF9};// This command set the IMU to calibrated and orientation mode
	char matrixandsc[9] = 		{0xFA, 0XFF, 0XD2, 0X04, 0X00, 0X00, 0X00, 0X09, 0X22};// matrix and sample counter configuration
	char Eulerandsc[9] = 		{0xFA, 0XFF, 0XD2, 0X04, 0X00, 0X00, 0X00, 0X05, 0X26};// Euler and sample counter configuration
	char startlogging[5] = 		{0xFA, 0XFF, 0X10, 0X00, 0XF1} ;// goes to measurement state.
	char outskipfactor[7] =		{0xFA, 0XFF, 0XD4, 0X02, 0X00, 0X09, 0X22}; // skipfactor of 9.
	char reqData[5] = 			{0xFA, 0XFF, 0x34, 0x00, 0xCD}; // Request data to be sent to the host.
	char setsamplerate[7] =     {0xFA, 0xFF, 0x04, 0x02, 0x04, 0x80, 0x77}; // set sample rate to default 100 Hz 

	//open IMU serial port
	IMU_Port = open(devicename_imu, O_RDWR | O_NOCTTY | O_NONBLOCK);
	
	//if unable to open port, exit out of program.
	if (IMU_Port < 0)
	{
		perror(devicename_imu);
		exit(-1);
	}
	
	tcgetattr(IMU_Port,&IMU_oldtio); // save current port settings 
	IMU_newtio.c_cflag = B115200  | CS8 | CLOCAL | CREAD;
	IMU_newtio.c_iflag = IGNPAR;
	IMU_newtio.c_oflag = 0;
	IMU_newtio.c_lflag = 0;
	IMU_newtio.c_cc[VMIN]=1;
	IMU_newtio.c_cc[VTIME]=0;
	tcflush(IMU_Port, TCIFLUSH);
	tcsetattr(IMU_Port,TCSANOW,&IMU_newtio);
	
	//send messages to imu to format data transmission
	
	usleep(100000);	
	write(IMU_Port, &startlogging, 5); // goes to measurement state.
	
	usleep(100000);
	write(IMU_Port, &gotoconfiguration, 5); // Go to Configuration State
	
	usleep(100000);
	write(IMU_Port, &caliorient, 7); // This command set the IMU to calibrated and orientation mode
	
	usleep(100000);
	write(IMU_Port, &Eulerandsc, 9); // matrix and sample counter configuration
	
	usleep(100000);
	write(IMU_Port, &setsamplerate, 7); //set sample rate to 100 Hz
	
	usleep(100000);
	write(IMU_Port, &outskipfactor, 7); // OUTSKIPFACTOR OF 09
	
	usleep(100000);
	write(IMU_Port, &startlogging, 5); // goes to measurement state.
	
	usleep(100000);
	
	//Open LCM Channels
	lcm_t * imu = lcm_create("udpm://239.255.76.67:7667?ttl=1");
    if(!imu)
        return -1;
		
	lcm_t * gps = lcm_create("udpm://239.255.76.67:7669?ttl=1");
	if(!gps)
		return -1;
		
	lcm_t * compass = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!compass)
		return -1;
		
	compass_compass_t_subscribe(compass, "COMPASS", &compass_handler, NULL);
	
	/////////////////////
	//Timeout Structure//
	/////////////////////
	
	struct timeval timeout = {0 , 5};
	
	//create IMU lcm struct for GPS and IMU
	imu_imu_t imu_lcm_struct;
	gps_gps_t gps_struct;
	 
	while(1) 
	{
		
		/////////////
		// Compass //
		/////////////
		
		int read_fd1_compass;
		read_fd1_compass = lcm_get_fileno(compass);
		fd_set fds1_compass;
		FD_ZERO(&fds1_compass);
		FD_SET(read_fd1_compass, &fds1_compass);
								  
		int status1_compass;
		status1_compass = select(read_fd1_compass + 1 , &fds1_compass,NULL,NULL, &timeout);
								 
		if( 0 == status1_compass){
		}else if(FD_ISSET(read_fd1_compass, &fds1_compass))
		{
		lcm_handle(compass);
		}
	
	
		char c_imu;
		fd_set fds;
		int ret1_IMU, ret2_IMU;
		
		//Zero file descriptor set, then append all types.
		FD_ZERO(&fds);
		//FD_SET(STDIN_FILENO, &fds);
		FD_SET(IMU_Port, &fds);
		
		ret1_IMU = select(IMU_Port + 2, &fds, NULL, NULL, NULL);
		
		if(ret1_IMU == -1) {
			perror("select");
			return 01;
		} else if (ret1_IMU > 0) {
			
			if(FD_ISSET(IMU_Port, &fds)) {
			do {
					ret2_IMU = read(IMU_Port, &c_imu, 1);
				} while (ret2_IMU < 0 && errno == EINTR);
	
				
				if(ret2_IMU == 1)
				{
					string_imu[index_imu] = c_imu;
					//printf("%x  %i", c_imu, index_imu);
					
			   		if (( string_imu[index_imu] == 0x32) & ( string_imu[index_imu-1] == 0xFF) & ( string_imu[index_imu-2] == 0xFA))
					{
						checksum_IMU =  0xFF;
						found_index_imu = index_imu;
						state_IMU = 1;
						
					} 
					
					if (state_IMU ==  1)
					{
						checksum_IMU =  checksum_IMU + string_imu[index_imu];
						checksum_IMU = checksum_IMU & 0x00ff;						
						//printf("\t checksum =  %x \r\n " , checksum_IMU);
						//check for checksum and correct length (data comes in from this imu at a fixed length, unlike the MTiG700)
						if ((checksum_IMU == 0x0000) && ( index_imu == found_index_imu + 76))
						{
							internal_status = 1;
						}
					}		
					if(internal_status == 1)
					{
							index_imu = 3;
							checksum_IMU = 0x02;
							internal_status = 0;
							imu_lcm_struct = imu_parse(string_imu, found_index_imu);
							
							//store gps waypoints as in gps structure
							gps_struct.latitude = imu_lcm_struct.lat;
							gps_struct.longitude = imu_lcm_struct.lon;
							
							
							///////////////////////////////////////////////////
							//Publishing IMU and GPS to Channel using LCM /////
							///////////////////////////////////////////////////
							imu_imu_t_publish(imu, "IMU", &imu_lcm_struct);
							gps_gps_t_publish(gps, "GPS", &gps_struct);
							/////////////////////////////////////////////////
							
					} else { 
							 index_imu++; 
							 
								if( index_imu > 198)
								{
									index_imu = 3;	
								}
							}					
				} else {
					fprintf(stderr, "\nnothing to read. probably port disconnected.\r\n");
					need_exit = -2;
				}
			}
		
		}
	
	
	
	}
	
	
	
	//Reset file attributes back to original state.
	tcsetattr(IMU_Port,TCSANOW,&IMU_oldtio);
	//tcsetattr(STDIN_FILENO,TCSANOW,&oldkey);
	return 0;
}

imu_imu_t imu_parse(char string_imu[200], int found_index_imu)
{
	extern float veh_heading;
	
	//create global copy of lcm struct
	imu_imu_t global_lcm_struct;

	int i;
	int m;
	int length;

	char  AccX[ 4 ] ;
	char  AccY[ 4 ] ;
	char  AccZ[ 4 ] ;
	char  GyrX[ 4 ] ;
	char  GyrY[ 4 ] ;
	char  GyrZ[ 4 ] ;
	char  MagX[ 4 ] ;
	char  MagY[ 4 ] ;
	char  MagZ[ 4 ] ;
	char  roll[ 4 ] ;
	char pitch[ 4 ] ;
	char   Yaw[ 4 ] ;
	char   lat[ 4 ];
	char   lon[ 4 ] ;
	char   alt[ 4 ] ;
	char   velx[ 4 ] ;
	char   vely[ 4 ] ;
	char   velz[ 4 ] ;
	unsigned int TS[ 2 ] ;

	number AccX1;
	number AccY1;
	number AccZ1;
	number GyrX1;
	number GyrY1;
	number GyrZ1;
	number MagX1;
	number MagY1;
	number MagZ1;
	number roll1;
	number pitch1;
	number Yaw1;
	number lat1;
	number lon1;
	number alt1;
	number velx1;
	number vely1;
	number velz1;

	m = found_index_imu;
	
	
	for(i = 0; i < 4; i = i + 1)
	{
		
		AccX1.word[i]	= string_imu[m + 5 - i];	
		AccY1.word[i] 	= string_imu[m + 9 - i];	
		AccZ1.word[i] 	= string_imu[m + 13 - i];	
		GyrX1.word[i] 	= string_imu[m + 17 - i];	
		GyrY1.word[i] 	= string_imu[m + 21 - i];	
		GyrZ1.word[i] 	= string_imu[m + 25 - i];	
		MagX1.word[i] 	= string_imu[m + 29 - i]; 	
		MagY1.word[i] 	= string_imu[m + 33 - i];	
		MagZ1.word[i] 	= string_imu[m + 37 - i];	
		roll1.word[i] 	= string_imu[m + 41 - i];	
		pitch1.word[i] 	= string_imu[m + 45 - i];	
		Yaw1.word[i] 	= string_imu[m + 49 - i];	
		
		lat1.word[i] 	= string_imu[m + 53 - i];	
		lon1.word[i] 	= string_imu[m + 57 - i];
		alt1.word[i] 	= string_imu[m + 61 - i];	
		velx1.word[i] 	= string_imu[m + 65 - i];	
		vely1.word[i] 	= string_imu[m + 69 - i];	
		velz1.word[i] 	= string_imu[m + 73 - i];	
		

	}
		//printf("\n");
		//printf("\n");
		
		
		/// Please Check for this for all the values.

		// printf(" AccX: %f \r\n", AccX1.f);	
		// printf(" AccY: %f \r\n", AccY1.f);	
		// printf(" AccZ: %f \r\n", AccZ1.f);	   
		// printf(" GyrX: %f \r\n", GyrX1.f);	
		// printf(" GyrY: %f \r\n", GyrY1.f);	
		// printf(" GyrZ: %f \r\n", GyrZ1.f);	
		// printf(" MagX: %f \r\n", MagX1.f);	
		// printf(" MagY: %f \r\n", MagY1.f);	
		// printf(" MagZ: %f \r\n", MagZ1.f);	
		// printf(" roll: %f \r\n", roll1.f);	
		// printf("pitch: %f \r\n", pitch1.f);	
		// printf("  Yaw: %f \r\n", Yaw1.f);	
		
		// printf("  lat: %f \r\n", lat1.f);
		// printf("  lon: %f \r\n", lon1.f);
		// printf("  alt: %f \r\n", alt1.f);
		// printf("  velx: %f \r\n", velx1.f);
		// printf("  vely: %f \r\n", vely1.f);
		// printf("  velz: %f \r\n", velz1.f);
		
		global_lcm_struct.AccX = AccX1.f;
		global_lcm_struct.AccY = AccY1.f;
		global_lcm_struct.AccZ = AccZ1.f;
		global_lcm_struct.GyrX = GyrX1.f;
		global_lcm_struct.GyrY = GyrY1.f;
		global_lcm_struct.GyrZ = GyrZ1.f;
		global_lcm_struct.MagX = MagX1.f;
		global_lcm_struct.MagY = MagY1.f;
		global_lcm_struct.MagZ = MagZ1.f;
		global_lcm_struct.roll = roll1.f;
		global_lcm_struct.pitch = pitch1.f;
		global_lcm_struct.alt = alt1.f;
		global_lcm_struct.lat = lat1.f;
		global_lcm_struct.lon = lon1.f;
		//convert from NWU to Body-Fixed
		global_lcm_struct.yaw = -1*Yaw1.f;
		global_lcm_struct.VelX = velx1.f*cos(veh_heading*3.14159/180.0)+-1*vely1.f*sin(veh_heading*3.14159/180.0);
		global_lcm_struct.VelY = -1*velx1.f*sin(veh_heading*3.14159/180.0)+-1*vely1.f*cos(veh_heading*3.14159/180.0);
		global_lcm_struct.VelZ = -1*velz1.f;
		
		
		
		return global_lcm_struct;
		
}
