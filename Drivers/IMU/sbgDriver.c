/*
	Original Author: Ivan Bertaska
	Last Updated by: Ivan Bertaska
	Last Updated: 06/11/2014
	
	This driver takes in values from the SBG IMU using the library provided with the DK. Must use the "SbgComSerial" library to compile and handle
	incoming data from IMU. Transmits data on IMU and GPS channels. Note: these will be used with a different version of the LCM packages to provide more
	information from the IMU. Be sure to use the RXGPS and RXIMU header files in conjunction with this source file.
	
	IN:
	
	OUT:
		IMU
		GPS
*/

# include <stdio.h>
# include <time.h>
# include <stdlib.h>
# include "sbgCom.h"
# include "RXimu_RXimu_t.h"

# define _DEBUG
# define _DIAGNOSTICS

# define HERTZ10 10
# define HERTZ5 20
# define HERTZ20 5
# define HERTZ50 2

typedef union conversion{
	double lf;
	unsigned char word[8];
	}conv;



// set up handlers for when there is data in the buffer and when there is an error
void continuousErrorCallback(SbgProtocolHandleInt *pHandler, SbgErrorCode errorCode, void *pUsrArg)
{
	char errorMsg[256];

	//
	// Convert our error code to a human readable error message
	//
	sbgComErrorToString(errorCode, errorMsg);

	//
	// Display an error message
	//
	
	fprintf(stderr,"continuousErrorCallback: We have received the following error %s\n", errorMsg);
	fflush(stderr);
}

// what to do when there is data in the file
void continuousCallback(SbgProtocolHandleInt *handler, SbgOutput *pOutput, void *pUsrArg)
{
	RXimu_RXimu_t localStruct;
	conv convLat, convLon, convTest, convTest2;
	int i;
	
	if (pOutput)
	{
		/////////////////////////////
		//DEBUG FOR DOUBLE POSITION//
		/////////////////////////////
		
		convLat.lf = pOutput->position[0];
		convLon.lf = pOutput->position[1];
		convTest.lf= pOutput->gpsLatitude*0.0000001;
		convTest2.lf= pOutput->gpsLongitude*0.0000001;
		
		// change convLat and convLon to by setting one bit from the first byte (last byte on little endian system) to zero
		convLat.word[7] ^= 0x01;
		convLon.word[7] ^= 0x01;
		
		#ifdef _DEBUG
		printf("%3.2f\t%3.2f\t%3.2f\n",	SBG_RAD_TO_DEG(pOutput->stateEuler[0]),
										SBG_RAD_TO_DEG(pOutput->stateEuler[1]),
										SBG_RAD_TO_DEG(pOutput->stateEuler[2]));
		printf("%3.2f\t%3.2f\t%3.2f\n", pOutput->gyroscopes[0],pOutput->gyroscopes[1],pOutput->gyroscopes[2]);
		printf("%3.2f\t%3.2f\t%3.2f\n", pOutput->accelerometers[0],pOutput->accelerometers[1],pOutput->accelerometers[2]);
		printf("%lf\t\t%lf\n", pOutput->position[0], pOutput->position[1]);
		printf("%d\t\t%d\n",pOutput->gpsLatitude, pOutput->gpsLongitude);
		printf("%u\n\n",pOutput->gpsNbSats);
		
		// print out bytes of each double position
		
		printf("Current reading from filtered position:\n");
		printf("%lf\t%lf\n",convLat.lf,convLon.lf);
		printf("[");
		for(i=0; i<8; i++)
		{
			printf("%x  ",convLat.word[7-i]);
		}
		printf("]\n");
		
		printf("[");
		for(i=0; i<8; i++)
		{
			printf("%x  ",convLon.word[7-i]);
		}
		printf("]\n\n");
		
		printf("%f\t%f\n",convTest.lf, convTest2.lf);
		printf("[");
		for(i=0; i<8; i++)
		{
			printf("%x  ",convTest.word[7-i]);
		}
		printf("]\n");
		
		printf("[");
		for(i=0; i<8; i++)
		{
			printf("%x  ",convTest2.word[7-i]);
		}
		printf("]\n\n");
		
		#endif
		
		// save to LCM structure
		localStruct.Acc[0] = pOutput->accelerometers[0];
		localStruct.Acc[1] = pOutput->accelerometers[1];
		localStruct.Acc[2] = pOutput->accelerometers[2];
		
		localStruct.Gyr[0] = pOutput->gyroscopes[0];
		localStruct.Gyr[1] = pOutput->gyroscopes[1];
		localStruct.Gyr[2] = pOutput->gyroscopes[2];
		
		localStruct.Mag[0] = pOutput->magnetometers[0];
		localStruct.Mag[1] = pOutput->magnetometers[1];
		localStruct.Mag[2] = pOutput->magnetometers[2];
		
		localStruct.Eul[0] = SBG_RAD_TO_DEG(pOutput->stateEuler[0]);
		localStruct.Eul[1] = SBG_RAD_TO_DEG(pOutput->stateEuler[1]);
		localStruct.Eul[2] = SBG_RAD_TO_DEG(pOutput->stateEuler[2]);
		
		localStruct.Pos[0] = convLat.lf;
		localStruct.Pos[1] = convLon.lf;
		localStruct.Pos[2] = 0;
		
		localStruct.PosAccur = pOutput->positionAccuracy;
		
		localStruct.Vel[0] = pOutput->velocity[0];
		localStruct.Vel[1] = pOutput->velocity[1];
		localStruct.Vel[2] = pOutput->velocity[2];
		
		localStruct.VelAccur = pOutput->velocityAccuracy;
		
		localStruct.Temp[0] = pOutput->temperatures[0];
		localStruct.Temp[1] = pOutput->temperatures[1];
		
		localStruct.gpsSat = pOutput->gpsNbSats;
		
		// Check for GPS fix
		localStruct.gpsFix = pOutput->gpsFlags & 0x02;
		
		// timestamp
		localStruct.utcYear = pOutput->utcYear;
		localStruct.utcMonth = pOutput->utcMonth;
		localStruct.utcDay = pOutput->utcDay;
		localStruct.utcHour = pOutput->utcHour;
		localStruct.utcMin = pOutput->utcMin;
		localStruct.utcSec = pOutput->utcSec;
		localStruct.utcNano = pOutput->utcNano;
		
		// publish data
		RXimu_RXimu_t_publish(pUsrArg, "IMU", &localStruct);
	}
}


int main()
{
  // initialize the protocol handle
  SbgProtocolHandle protocolHandle;
  uint8 OutputMode;

  printf("\n\n\tsbgDriver INITIALIZATION\n");
  
  //////////////////////
  //LCM INITIALIZATION//
  //////////////////////
  
  //Open LCM Channels
  lcm_t * imu = lcm_create("udpm://239.255.76.67:7667?ttl=1");
  if(!imu)
  {
	printf("\tPROBLEM OPENING UP LCM CHANNEL\n\n");
    return -1;
  }
  printf("\tLCM CHANNEL: IMU\n");
  printf("\tSUCCESS!\n");
  
  //////////////////////////////
  //OPEN SBG USING COM LIBRARY//
  //////////////////////////////
  
  // open serial port
  if (sbgComInit("/dev/ttts0", 115200, &protocolHandle) == SBG_NO_ERROR)
	{
		sbgSleep(50);
		printf("\n\tSBG IMU INITIALIZED: ON\n");
		
		#ifdef _DIAGNOSTICS
		sbgGetOutputMode(protocolHandle,&OutputMode);
		if(OutputMode&SBG_OUTPUT_MODE_BIG_ENDIAN)
			printf("\tIMU IS IN BIG ENDIAN MODE\n\n");
		else if(OutputMode&SBG_OUTPUT_MODE_LITTLE_ENDIAN)
			printf("\tIMU IS IN LITTLE ENDIAN MODE\n\n");
		else
			printf("\tERROR IN DIAGNOSTIC TOOLS\n\n");
		if(OutputMode&SBG_OUTPUT_MODE_FLOAT)
			printf("\tIMU IS IN FLOAT OUTPUT MODE\n\n");
		else if(OutputMode&SBG_OUTPUT_MODE_FIXED)
			printf("\tIMU IS IN FIXED OUTPUT MODE\n\n");
		else 
			printf("\tERROR IN DIAGNOSTIC TOOLS\n\n");
		#endif
		
		////////////////////////////////////
		// FOLLOWING SECTION DOES NOT WORK//
		////////////////////////////////////
		
		// provides output of error with a warning of a "small buffer" that is overflowed.
		// use default configuration from sbgCenter for now
		
		#ifdef _sbgSETUP
		printf("\tCONFIGURING SYSTEM TO SPECIFIED OUTPUTS\n\n");
		
		// setup the proper outputs from the system
		if(sbgSetDefaultOutputMask(protocolHandle, SBG_OUTPUT_QUATERNION | SBG_OUTPUT_EULER | SBG_OUTPUT_GYROSCOPES | SBG_OUTPUT_ACCELEROMETERS | SBG_OUTPUT_TEMPERATURES | SBG_OUTPUT_POSITION | SBG_OUTPUT_VELOCITY | SBG_OUTPUT_NAV_ACCURACY) != SBG_NO_ERROR)
			printf("\tUNABLE TO PROPERLY CONFIGURE SBG WITH DATA OUTPUTS\n");
			
		sbgSleep(10);
		
		// setup little endian
		if(sbgSetOutputMode(protocolHandle, SBG_OUTPUT_MODE_LITTLE_ENDIAN | SBG_OUTPUT_MODE_FLOAT) != SBG_NO_ERROR)
			printf("\tUNABLE TO CONFIGURE SBG TO LITTLE ENDIAN MODE\n"); 
		sbgSleep(10);
		
		// set up continuous mode at 10 HZ
		if(sbgSetContinuousMode(protocolHandle,SBG_CONTINUOUS_MODE_ENABLE, HERTZ10) != SBG_NO_ERROR)
			printf("\tUNABLE TO CONFIGURE SBG TO CONTINUOUS MODE\n");
		sbgSleep(10);
		
		printf("\tEND OF SBG CONFIGURATION\n");
		
		#endif
		
		/////////////////////////
		//END OF BROKEN SECTION//
		/////////////////////////
		
		sbgSetContinuousErrorCallback(protocolHandle, continuousErrorCallback, NULL);
		sbgSetContinuousModeCallback(protocolHandle, continuousCallback, imu);
		
		//
		// Main loop
		//
		while (1)
		{
			//
			// Handle our continuous mode system
			//
			sbgProtocolContinuousModeHandle(protocolHandle);
	
			//
			// Small pause to unload CPU
			//
			sbgSleep(10);
		}

		//
		// Close our protocol system
		//
		sbgProtocolClose(protocolHandle);

		return 0;
	}
	else
	{
		fprintf(stderr, "Unable to open IG-500 device\n");
		fflush(stderr);
		return -1;
	}
	
}
