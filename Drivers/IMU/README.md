# IMU Driver

There are two distinct inertial measurement units (IMUs) used in this project. There is the SBG IG-500N unit as well as an Xsens MTi-G unit. These two differ in the amount of information given as well as the accuracy of that information. There are two different LCM packages for each. 

## SBG IG-500N

The IG-500N transmits all of its information in one channel ("IMU") rather than several channels as in the Xsens.

* Serial Port: */dev/ttts0*
* Channel : "IMU"
* Address : 239.255.76.67:7667

* LCM Package: 

```
#!c

package RXimu;

struct RXimu_t
{
	// filtered data
	float Acc[3];
	float Gyr[3];
	float Mag[3];
	float Eul[3]; // euler angles roll, pitch, yaw
	float Temp[2]; // internal temperature of IMU
	double Pos[3]; // [lat lon alt (in m)]
	float PosAccur; // position accuracy
	float Vel[3]; // velocity in surge, sway, and heave
	float VelAccur; // velocity accuracy
	
	// gps information
	int8_t gpsFix; // 0 off - 1 on
	int8_t gpsSat; // no of satellites
	
	// timestamp (from GPS)
	int8_t utcYear;
	int8_t utcMonth;
	int8_t utcDay;
	int8_t utcHour;
	int8_t utcMin;
	int8_t utcSec;
	int32_t utcNano;
}
```

This file can be made by using the following command within the pertaining directory:

```
#!bash

user@ts7800 /path/to/Drivers/IMU>> make -f sbgMakefile

```

## Xsens MTi-G

The Xsens MTi-G transmits its information in two different LCM channels. This is because of the legacy driver written for it from a previous project. It transmits all inertial information as well as the position information in the "IMU" channel. It also transmits the same position data in a channel called "GPS".

* Serial Port: */dev/ttts0*
* Channel : "IMU" and "GPS"
* Address : "IMU" - 239.255.76.67:7667  "GPS" - 239.255.76.67:7669

* "IMU" Channel LCM Package:

```
#!c

package imu;

struct imu_t
{
	float AccX;
	float AccY;
	float AccZ;
	float GyrX;
	float GyrY;
	float GyrZ;
	float MagX;
	float MagY;
	float MagZ;
	float roll;
	float pitch;
	float yaw;
	float alt;
	float lat;
	float lon;
	float VelX;
	float VelY;
	float VelZ;
}

```

* "GPS" Channel LCM Structure:

```
#!c

package gps;

struct gps_t
{
	float latitude;
	float longitude;
}

```

This project can be made by the following command in the bash shell:

```
#!bash

user@ts7800 /path/to/Drivers/IMU>> make -f xsensMakefile

```