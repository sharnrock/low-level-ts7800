#define _BSD_SOURCE

#include <stdio.h>
#include <lcm/lcm.h>
#include <sys/select.h>
#include <math.h>
#include <time.h>
#include <libconfig.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <errno.h>
#include <error.h>
#include "global.h"
#include <stdint.h>


//LCM header files
#include "RXState_RXState_t.h"
#include "lidarPanda_visionInfo_t.h"
#include "Speedgoat_sgInfo_t.h"


#define BAUDRATE B9600
#define _POSIX_SOURCE 1 /* POSIX compliant source */

void writeSerialMessageFromStateStruct(RXState_RXState_t stateStruct,
                                       char * messageBuffer,
                                       int bufferSize);


static void stateFunc(const lcm_recv_buf_t *rbuf, const char * channel, 
        const RXState_RXState_t * msg, void * user_data) {	
    memcpy(user_data,msg,sizeof(RXState_RXState_t));

}

static void visionInfoFunc(const lcm_recv_buf_t *rbuf, const char * channel, 
        const lidarPanda_visionInfo_t * msg, void * user_data) {
    memcpy(user_data,msg,sizeof(lidarPanda_visionInfo_t));
}

static void sgInfoFunc(const lcm_recv_buf_t *rbuf, const char * channel, 
        const Speedgoat_sgInfo_t * msg, void * user_data) {	
    memcpy(user_data,msg,sizeof(Speedgoat_sgInfo_t));
}
