/*
	Original Author: Michael Kaiser-Cross
	Last Updated by: Michael Kaiser-Cross
	Last Updated: 09/21/2014
	
	This file is used to transmit vehicle state information to the ground station over RF
	
	LCM Channels:
	IN:
		STATE (PLANNER_STATE ?)
	OUT:

        
        todo - recieving state from planner to include in transmission        
        
*/
#include "GSMessenger.h"
/*
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',  
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'}; */
static char encoding_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length) {

  *output_length = 4 * ((input_length + 2) / 3);

  char *encoded_data = malloc(*output_length);
  if (encoded_data == NULL) return NULL;

  for (int i = 0, j = 0; i < input_length;) {

    uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
    uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
    uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

    uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

    encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
    encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
    encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
    encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
  }

  for (int i = 0; i < mod_table[input_length % 3]; i++)
    encoded_data[*output_length - 1 - i] = '=';

  return encoded_data;
}


void build_decoding_table() {

  decoding_table = malloc(256);

  for (int i = 0; i < 64; i++)
    decoding_table[(unsigned char) encoding_table[i]] = i;
}


void base64_cleanup() {
  free(decoding_table);
}



unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {

  if (decoding_table == NULL) build_decoding_table();

  if (input_length % 4 != 0) return NULL;

  *output_length = input_length / 4 * 3;
  if (data[input_length - 1] == '=') (*output_length)--;
  if (data[input_length - 2] == '=') (*output_length)--;

  unsigned char *decoded_data = malloc(*output_length);
  if (decoded_data == NULL) return NULL;

  for (int i = 0, j = 0; i < input_length;) {

    uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
    uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
    uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
    uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

    uint32_t triple = (sextet_a << 3 * 6)
      + (sextet_b << 2 * 6)
      + (sextet_c << 1 * 6)
      + (sextet_d << 0 * 6);

    if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
    if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
    if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
  }

  return decoded_data;
}


void clearStateStruct(RXState_RXState_t * ss) {
  ss->USV_pose[0] = 0;
  ss->USV_pose[1] = 0;
  ss->USV_pose[2] = 0;
  ss->USV_vel[0] = 0;
  ss->USV_vel[1] = 0;
  ss->USV_vel[2] = 0;
  ss->USV_euler[0] = 0;
  ss->USV_euler[1] = 0;
  ss->USV_euler[2] = 0;
  ss->GeoCoord[0] = 0;
  ss->GeoCoord[1] = 0;
  ss->PortState[0] = 0;
  ss->PortState[1] = 0;
  ss->StbdState[0] = 0;
  ss->StbdState[1] = 0;
  ss->StartTime = -1;
  ss->ElapsedTime = -1;
  ss->RCmode = -1;
}

int main() {

  ////////////////////////
  // Open serial device //
  ////////////////////////
  int fd;
  const char * modemFilenameString = "/dev/ttts8"; //"/dev/ttyACM0"; 
  fd = open(modemFilenameString, O_RDWR | O_NOCTTY);

  if (errno == EACCES) printf("permission denied. Are you running as superuser?");
  if (fd < 0) {
    printf("could not open serial device\n");
    exit(-1);
  }
  printf("serial device: %s\n\n", modemFilenameString);
  // set up termios settings
  int c, res;
  struct termios oldtio,newtio;
  tcgetattr(fd,&oldtio); /* save current port settings */
  bzero(&newtio, sizeof(newtio));
  newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD | (~ICANON);
  newtio.c_iflag = IGNPAR;
  newtio.c_oflag = OPOST ;
  // set input mode (non-canonical, no echo,...) 
  newtio.c_lflag = 0;
  newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused */
  newtio.c_cc[VMIN]     = 5;   /* blocking read until 5 chars received */
  // write configuration
  tcflush(fd, TCIFLUSH);
  tcsetattr(fd,TCSANOW,&newtio);

  ///////////////////////
  // Open LCM Channels //
  ///////////////////////

  // Lcm message structs
  RXState_RXState_t stateStruct;
  lidarPanda_visionInfo_t visionInfoStruct;
  Speedgoat_sgInfo_t sgInfoStruct;

  printf("Size of RXState_RXState_t = %d\n", (int) sizeof(RXState_RXState_t));
  printf("Size of visionInfoStruct = %d\n",  (int) sizeof(visionInfoStruct));
  printf("Size of sgInfoStruct = %d\n",      (int) sizeof(sgInfoStruct));

  // Create LCM Handles
  lcm_t * lcmState = lcm_create("udpm://239.255.76.67:7667?ttl=1");
  lcm_t * lcmVisionInfo = lcm_create("udpm://239.255.76.67:7667?ttl=1");
  lcm_t * lcmSgInfo = lcm_create("udpm://239.255.76.67:7667?ttl=1");

  // Check if LCM handle is good
  if (!lcmState || !lcmVisionInfo || !lcmSgInfo) printf("LCM initialization error\n");

  // Subscribe to LCM Channels
  RXState_RXState_t_subscription_t * stateSubscription =  
  RXState_RXState_t_subscribe(lcmState, "STATE", &stateFunc, &stateStruct);
  lidarPanda_visionInfo_t_subscription_t * visionInfoSubscription = 
  lidarPanda_visionInfo_t_subscribe(lcmVisionInfo, "vision", &visionInfoFunc, &visionInfoStruct);
  Speedgoat_sgInfo_t_subscription_t * sgInfoSubscription =  
  Speedgoat_sgInfo_t_subscribe(lcmSgInfo, "planner", &sgInfoFunc, &sgInfoStruct);

  // Set queue to only hold one value
  RXState_RXState_t_subscription_set_queue_capacity(stateSubscription, 1); 
  lidarPanda_visionInfo_t_subscription_set_queue_capacity(visionInfoSubscription, 1); 
  Speedgoat_sgInfo_t_subscription_set_queue_capacity(sgInfoSubscription, 1); 

  printf("\tOPENED LCM CHANNELS SUCCESSFULLY\n\n");

  struct timeval timeout = {0,1}; // wait for milliseconds for message
  int msg = 0;
  printf("\tWAITING FOR LCM CHANNEL MESSAGES\n\n");

  ///////////////////////////////////////////////////////////////////////////
  // Event loop initialization
  // Listen for lcm messages, convert to serial message, send to rf device
  ///////////////////////////////////////////////////////////////////////////

  // buffers to build serial string
  unsigned char messageBuffer[400000];
  size_t messageBufferLen = 400000;
  size_t messageBufferPosition;
  size_t base64BufferLen;
  unsigned char * base64Buffer;
  unsigned char * translatedBuffer;
  int translatedBufferLen;
  RXState_RXState_t secondaryStateStruct;
  unsigned char newLineBuffer[2];

  newLineBuffer[0] = '\r';  newLineBuffer[1] = '\n';
  usleep (1000000);
  clearStateStruct(&stateStruct);

  int count = 0;

  while(1) {
    count++;
    printf("count = %d\n", count);
    //memset(messageBuffer, 0, messageBufferSize);
    printf("test\n");
    messageBufferPosition = 0;
    msg = LCMCheckForMessage(lcmState, &timeout);
    if(msg == 1) {
      lcm_handle(lcmState);
      printf("recieved state message(%d)\n", (int)base64BufferLen);
    }
    memcpy(messageBuffer, &stateStruct, sizeof(stateStruct));
    messageBufferLen = sizeof(stateStruct);
    base64Buffer = base64_encode(messageBuffer, messageBufferLen, &base64BufferLen);
    write(fd, base64Buffer + messageBufferPosition, 100);
    usleep(166660);
    messageBufferPosition += 100;
    write(fd, base64Buffer + messageBufferPosition, base64BufferLen-messageBufferPosition);
    usleep(166660);
    free(base64Buffer);



    messageBufferPosition = 0;
    msg = LCMCheckForMessage(lcmVisionInfo, &timeout);
    if(msg == 1) {
      lcm_handle(lcmVisionInfo);
      printf("recieved visioninfo message(%d)\n", (int)base64BufferLen);
    }
    memcpy(messageBuffer, &visionInfoStruct, sizeof(visionInfoStruct));
    messageBufferLen = sizeof(visionInfoStruct);
    base64Buffer = base64_encode(messageBuffer, messageBufferLen, &base64BufferLen);
    write(fd, base64Buffer + messageBufferPosition, 100);
    usleep(166660);
    messageBufferPosition += 100;
    write(fd, base64Buffer + messageBufferPosition, base64BufferLen-messageBufferPosition);
    usleep(166660);
    free(base64Buffer);

    messageBufferPosition = 0;
    msg = LCMCheckForMessage(lcmSgInfo, &timeout);
    if(msg == 1) {
      lcm_handle(lcmSgInfo);
      printf("recieved sginfo message(%d)\n", (int)base64BufferLen);
    }

    printf("mem\n");
    memcpy(messageBuffer, &sgInfoStruct, sizeof(sgInfoStruct));
    messageBufferLen = sizeof(sgInfoStruct);
    base64Buffer = base64_encode(messageBuffer, messageBufferLen, &base64BufferLen);

    printf("write\n");
    write(fd, base64Buffer + messageBufferPosition, 100);
    usleep(166660);
    tcflush(fd, TCIOFLUSH);
    printf("success\n");
    messageBufferPosition += 100;

    printf("write2\n");
    write(fd, base64Buffer + messageBufferPosition, base64BufferLen-messageBufferPosition);
    usleep(166660);

    printf("freeing\n");
    free(base64Buffer);

    printf("writing new line\n");

    write(fd, newLineBuffer, 2);
    usleep(1000);
    /*
    printf("\nmessageBuffer (%d) =\n", base64BufferLen);
    for (int i = 0; i < messageBufferLen; i++) {
      printf("%d ", messageBuffer[i]);
    }
    printf("\n");
    printf("base64Buffer (%d) =\n", base64BufferLen);
    for (int i = 0; i < base64BufferLen; i++) {
      printf("%c ", base64Buffer[i]);
    }
    printf("\n");
    */
  }

  return -1;

}                  
/*

  void writeSerialMessageFromStateStruct(RXState_RXState_t stateStruct,
                                         char * messageBuffer,
                                         int bufferSize) {
    char temporaryBuffer[255];
    int temporaryBufferSize = 255;
    temporaryBuffer[0] = '\0';
    sprintf(temporaryBuffer, "$");
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "%f%f%f", 
            stateStruct.USV_pose[0],
            stateStruct.USV_pose[1],
	    stateStruct.USV_pose[2]);
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "%f%f%f", 
            stateStruct.USV_vel[0],
            stateStruct.USV_vel[1],
	    stateStruct.USV_vel[2]);
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "%f%f%f", 
            stateStruct.USV_euler[0],
            stateStruct.USV_euler[1],
	    stateStruct.USV_euler[2]);
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "%f%f", 
            stateStruct.GeoCoord[0],
            stateStruct.GeoCoord[1]);
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "%f%f", 
            stateStruct.PortState[0],
            stateStruct.PortState[1]);
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "%f%f", 
            stateStruct.StbdState[0],
            stateStruct.StbdState[1]);
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "%f", 
            stateStruct.StartTime);
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "%f", 
            stateStruct.ElapsedTime);
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "%d", 
            (int) stateStruct.RCmode);
    strcat(messageBuffer, temporaryBuffer);
    sprintf(temporaryBuffer, "\r\n");
    strcat(messageBuffer, temporaryBuffer);
  }

*/
