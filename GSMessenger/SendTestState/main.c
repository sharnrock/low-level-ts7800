/*
	Original Author: Michael Kaiser-Cross
	Last Updated by: Michael Kaiser-Cross
	Last Updated: 09/21/2014
	
	This file is used to transmit vehicle state information to the ground station over RF
	
	LCM Channels:
	IN:
		STATE (PLANNER_STATE ?)
	OUT:

        
        todo - recieving state from planner to include in transmission        
        
*/
#include "main.h"

void clearStateStruct(RXState_RXState_t * ss) {
  ss->USV_pose[0] = 1;
  ss->USV_pose[1] = 2;
  ss->USV_pose[2] = 3;
  ss->USV_vel[0] = 1;
  ss->USV_vel[1] = 2;
  ss->USV_vel[2] = 3;
  ss->USV_euler[0] = 1;
  ss->USV_euler[1] = 2;
  ss->USV_euler[2] = 3;
  ss->GeoCoord[0] = 120.123456;
  ss->GeoCoord[1] = 194.123456;
  ss->PortState[0] = 0;
  ss->PortState[1] = 0;
  ss->StbdState[0] = 0;
  ss->StbdState[1] = 0;
  ss->StartTime = -1;
  ss->ElapsedTime = -1;
  ss->RCmode = 1;
}
void clearVisionInfoStruct(lidarPanda_visionInfo_t * ss) {
  for (int i = 0; i < 12; i++) ss->distances[i] = i;
  for (int i = 0; i < 12; i++) ss->angles[i] = i*10;
  for (int i = 0; i < 3; i++) ss->colorSeq[i] = i*32;
  ss->dockSymbol = 1;
}
void clearSgInfoStruct(Speedgoat_sgInfo_t * ss) {
  for (int i = 0; i < 12; i++) ss->slamMap[i] = i % 2 ? i*1.4 : 10-i*0.4;
  ss->dockPos[0] = 5;
  ss->dockPos[1] = 5;
  ss->taskNum = 0;
  ss->sgTime = 0;
}

int main() {
  ///////////////////////
  // Open LCM Channels //
  ///////////////////////

  // Lcm message structs
  RXState_RXState_t stateStruct;
  lidarPanda_visionInfo_t visionInfoStruct;
  Speedgoat_sgInfo_t sgInfoStruct;

  // Create LCM Handles
  lcm_t * lcmState = lcm_create("udpm://239.255.76.67:7667?ttl=1");
  lcm_t * lcmVisionInfo = lcm_create("udpm://239.255.76.67:7667?ttl=1");
  lcm_t * lcmSgInfo = lcm_create("udpm://239.255.76.67:7667?ttl=1");


  // Check if LCM handle is good


  ///////////////////////////////////////////////////////////////////////////
  // Event loop initialization
  // Send repeated messages
  ///////////////////////////////////////////////////////////////////////////

  printf("Entering loop");
  RXState_RXState_t secondaryStateStruct;
  clearStateStruct(&stateStruct);
  clearVisionInfoStruct(&visionInfoStruct);
  clearSgInfoStruct(&sgInfoStruct);

  while(1) {

    RXState_RXState_t_publish(lcmState, "STATE", &stateStruct);
    //printf("sent state message\n");
    usleep(50000);
    lidarPanda_visionInfo_t_publish(lcmVisionInfo, "vision", &visionInfoStruct);
    //printf("sent vision message\n");
    usleep(50000);
    Speedgoat_sgInfo_t_publish(lcmSgInfo, "planner", &sgInfoStruct);
    //printf("sent planner message\n");
    usleep(50000);

    
  }
  return -1;
}                  


