#include <stdio.h>
#include <lcm/lcm.h>
#include <sys/select.h>
#include <math.h>
#include <time.h>
#include <libconfig.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <errno.h>
#include <error.h>
#include "global.h"

//LCM header files
#include "RXState_RXState_t.h"
#include "lidarPanda_visionInfo_t.h"
#include "Speedgoat_sgInfo_t.h"



#define BAUDRATE B9600
#define _POSIX_SOURCE 1 /* POSIX compliant source */

