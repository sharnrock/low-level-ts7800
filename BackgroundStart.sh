#!/bin/bash

# clean up process
function cleanup() {
  for pid in ${pids[@]}
  do
    echo "killing process: $pid"
    kill $pid
  done
}

# set up trap option to clean up on exit
trap 'cleanup; exit' SIGHUP SIGINT SIGTERM

# start compass driver
nohup Drivers/Compass/CompassDriver &
pids[0]=$!
echo Compass driver started!
echo Process ID = ${pids[0]}
echo 

# start IMU driver
nohup Drivers/IMU/MTiGDriver &
pids[1]=$!
echo IMU driver started!
echo Process ID = ${pids[1]}
echo 

# start RX State
nohup State/RXState &
pids[2]=$!
echo RXState started! 
echo Process ID = ${pids[1]}
echo 
sleep 1

nohup Maneuvering/RXManeuvering &
pids[3]=$!
echo RXManeuvering started!
echo Process ID = ${pids[1]}
echo

echo All background processes have been started. Start main executable RXControl.

wait ${pids[3]} 


