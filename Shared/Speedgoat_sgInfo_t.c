/** THIS IS AN AUTOMATICALLY GENERATED FILE.  DO NOT MODIFY
 * BY HAND!!
 *
 * Generated by lcm-gen
 **/

#include <string.h>
#include "Speedgoat_sgInfo_t.h"

static int __Speedgoat_sgInfo_t_hash_computed;
static int64_t __Speedgoat_sgInfo_t_hash;

int64_t __Speedgoat_sgInfo_t_hash_recursive(const __lcm_hash_ptr *p)
{
    const __lcm_hash_ptr *fp;
    for (fp = p; fp != NULL; fp = fp->parent)
        if (fp->v == __Speedgoat_sgInfo_t_get_hash)
            return 0;

    const __lcm_hash_ptr cp = { p, (void*)__Speedgoat_sgInfo_t_get_hash };
    (void) cp;

    int64_t hash = 0x72f2b6fc5bc657c1LL
         + __float_hash_recursive(&cp)
         + __float_hash_recursive(&cp)
         + __float_hash_recursive(&cp)
         + __int8_t_hash_recursive(&cp)
         + __float_hash_recursive(&cp)
        ;

    return (hash<<1) + ((hash>>63)&1);
}

int64_t __Speedgoat_sgInfo_t_get_hash(void)
{
    if (!__Speedgoat_sgInfo_t_hash_computed) {
        __Speedgoat_sgInfo_t_hash = __Speedgoat_sgInfo_t_hash_recursive(NULL);
        __Speedgoat_sgInfo_t_hash_computed = 1;
    }

    return __Speedgoat_sgInfo_t_hash;
}

int __Speedgoat_sgInfo_t_encode_array(void *buf, int offset, int maxlen, const Speedgoat_sgInfo_t *p, int elements)
{
    int pos = 0, thislen, element;

    for (element = 0; element < elements; element++) {

        thislen = __float_encode_array(buf, offset + pos, maxlen - pos, p[element].slamMap, 24);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __float_encode_array(buf, offset + pos, maxlen - pos, p[element].acousticPos, 2);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __float_encode_array(buf, offset + pos, maxlen - pos, p[element].dockPos, 2);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __int8_t_encode_array(buf, offset + pos, maxlen - pos, &(p[element].taskNum), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __float_encode_array(buf, offset + pos, maxlen - pos, &(p[element].sgTime), 1);
        if (thislen < 0) return thislen; else pos += thislen;

    }
    return pos;
}

int Speedgoat_sgInfo_t_encode(void *buf, int offset, int maxlen, const Speedgoat_sgInfo_t *p)
{
    int pos = 0, thislen;
    int64_t hash = __Speedgoat_sgInfo_t_get_hash();

    thislen = __int64_t_encode_array(buf, offset + pos, maxlen - pos, &hash, 1);
    if (thislen < 0) return thislen; else pos += thislen;

    thislen = __Speedgoat_sgInfo_t_encode_array(buf, offset + pos, maxlen - pos, p, 1);
    if (thislen < 0) return thislen; else pos += thislen;

    return pos;
}

int __Speedgoat_sgInfo_t_encoded_array_size(const Speedgoat_sgInfo_t *p, int elements)
{
    int size = 0, element;
    for (element = 0; element < elements; element++) {

        size += __float_encoded_array_size(p[element].slamMap, 24);

        size += __float_encoded_array_size(p[element].acousticPos, 2);

        size += __float_encoded_array_size(p[element].dockPos, 2);

        size += __int8_t_encoded_array_size(&(p[element].taskNum), 1);

        size += __float_encoded_array_size(&(p[element].sgTime), 1);

    }
    return size;
}

int Speedgoat_sgInfo_t_encoded_size(const Speedgoat_sgInfo_t *p)
{
    return 8 + __Speedgoat_sgInfo_t_encoded_array_size(p, 1);
}

int __Speedgoat_sgInfo_t_decode_array(const void *buf, int offset, int maxlen, Speedgoat_sgInfo_t *p, int elements)
{
    int pos = 0, thislen, element;

    for (element = 0; element < elements; element++) {

        thislen = __float_decode_array(buf, offset + pos, maxlen - pos, p[element].slamMap, 24);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __float_decode_array(buf, offset + pos, maxlen - pos, p[element].acousticPos, 2);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __float_decode_array(buf, offset + pos, maxlen - pos, p[element].dockPos, 2);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __int8_t_decode_array(buf, offset + pos, maxlen - pos, &(p[element].taskNum), 1);
        if (thislen < 0) return thislen; else pos += thislen;

        thislen = __float_decode_array(buf, offset + pos, maxlen - pos, &(p[element].sgTime), 1);
        if (thislen < 0) return thislen; else pos += thislen;

    }
    return pos;
}

int __Speedgoat_sgInfo_t_decode_array_cleanup(Speedgoat_sgInfo_t *p, int elements)
{
    int element;
    for (element = 0; element < elements; element++) {

        __float_decode_array_cleanup(p[element].slamMap, 24);

        __float_decode_array_cleanup(p[element].acousticPos, 2);

        __float_decode_array_cleanup(p[element].dockPos, 2);

        __int8_t_decode_array_cleanup(&(p[element].taskNum), 1);

        __float_decode_array_cleanup(&(p[element].sgTime), 1);

    }
    return 0;
}

int Speedgoat_sgInfo_t_decode(const void *buf, int offset, int maxlen, Speedgoat_sgInfo_t *p)
{
    int pos = 0, thislen;
    int64_t hash = __Speedgoat_sgInfo_t_get_hash();

    int64_t this_hash;
    thislen = __int64_t_decode_array(buf, offset + pos, maxlen - pos, &this_hash, 1);
    if (thislen < 0) return thislen; else pos += thislen;
    if (this_hash != hash) return -1;

    thislen = __Speedgoat_sgInfo_t_decode_array(buf, offset + pos, maxlen - pos, p, 1);
    if (thislen < 0) return thislen; else pos += thislen;

    return pos;
}

int Speedgoat_sgInfo_t_decode_cleanup(Speedgoat_sgInfo_t *p)
{
    return __Speedgoat_sgInfo_t_decode_array_cleanup(p, 1);
}

int __Speedgoat_sgInfo_t_clone_array(const Speedgoat_sgInfo_t *p, Speedgoat_sgInfo_t *q, int elements)
{
    int element;
    for (element = 0; element < elements; element++) {

        __float_clone_array(p[element].slamMap, q[element].slamMap, 24);

        __float_clone_array(p[element].acousticPos, q[element].acousticPos, 2);

        __float_clone_array(p[element].dockPos, q[element].dockPos, 2);

        __int8_t_clone_array(&(p[element].taskNum), &(q[element].taskNum), 1);

        __float_clone_array(&(p[element].sgTime), &(q[element].sgTime), 1);

    }
    return 0;
}

Speedgoat_sgInfo_t *Speedgoat_sgInfo_t_copy(const Speedgoat_sgInfo_t *p)
{
    Speedgoat_sgInfo_t *q = (Speedgoat_sgInfo_t*) malloc(sizeof(Speedgoat_sgInfo_t));
    __Speedgoat_sgInfo_t_clone_array(p, q, 1);
    return q;
}

void Speedgoat_sgInfo_t_destroy(Speedgoat_sgInfo_t *p)
{
    __Speedgoat_sgInfo_t_decode_array_cleanup(p, 1);
    free(p);
}

int Speedgoat_sgInfo_t_publish(lcm_t *lc, const char *channel, const Speedgoat_sgInfo_t *p)
{
      int max_data_size = Speedgoat_sgInfo_t_encoded_size (p);
      uint8_t *buf = (uint8_t*) malloc (max_data_size);
      if (!buf) return -1;
      int data_size = Speedgoat_sgInfo_t_encode (buf, 0, max_data_size, p);
      if (data_size < 0) {
          free (buf);
          return data_size;
      }
      int status = lcm_publish (lc, channel, buf, data_size);
      free (buf);
      return status;
}

struct _Speedgoat_sgInfo_t_subscription_t {
    Speedgoat_sgInfo_t_handler_t user_handler;
    void *userdata;
    lcm_subscription_t *lc_h;
};
static
void Speedgoat_sgInfo_t_handler_stub (const lcm_recv_buf_t *rbuf,
                            const char *channel, void *userdata)
{
    int status;
    Speedgoat_sgInfo_t p;
    memset(&p, 0, sizeof(Speedgoat_sgInfo_t));
    status = Speedgoat_sgInfo_t_decode (rbuf->data, 0, rbuf->data_size, &p);
    if (status < 0) {
        fprintf (stderr, "error %d decoding Speedgoat_sgInfo_t!!!\n", status);
        return;
    }

    Speedgoat_sgInfo_t_subscription_t *h = (Speedgoat_sgInfo_t_subscription_t*) userdata;
    h->user_handler (rbuf, channel, &p, h->userdata);

    Speedgoat_sgInfo_t_decode_cleanup (&p);
}

Speedgoat_sgInfo_t_subscription_t* Speedgoat_sgInfo_t_subscribe (lcm_t *lcm,
                    const char *channel,
                    Speedgoat_sgInfo_t_handler_t f, void *userdata)
{
    Speedgoat_sgInfo_t_subscription_t *n = (Speedgoat_sgInfo_t_subscription_t*)
                       malloc(sizeof(Speedgoat_sgInfo_t_subscription_t));
    n->user_handler = f;
    n->userdata = userdata;
    n->lc_h = lcm_subscribe (lcm, channel,
                                 Speedgoat_sgInfo_t_handler_stub, n);
    if (n->lc_h == NULL) {
        fprintf (stderr,"couldn't reg Speedgoat_sgInfo_t LCM handler!\n");
        free (n);
        return NULL;
    }
    return n;
}

int Speedgoat_sgInfo_t_subscription_set_queue_capacity (Speedgoat_sgInfo_t_subscription_t* subs,
                              int num_messages)
{
    return lcm_subscription_set_queue_capacity (subs->lc_h, num_messages);
}

int Speedgoat_sgInfo_t_unsubscribe(lcm_t *lcm, Speedgoat_sgInfo_t_subscription_t* hid)
{
    int status = lcm_unsubscribe (lcm, hid->lc_h);
    if (0 != status) {
        fprintf(stderr,
           "couldn't unsubscribe Speedgoat_sgInfo_t_handler %p!\n", hid);
        return -1;
    }
    free (hid);
    return 0;
}

