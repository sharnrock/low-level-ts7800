# include <sys/time.h>
# include <math.h>

// common defines
# define pi 3.14159265359

// north lake reference points
# define LATREF 1.287926
# define LONREF 103.858440


	///////////////////////
	//POSE TRANSFORMATION//
	///////////////////////
	
typedef struct {
	float xt;
	float yt;
	float ut;
	float vt;
} POSEstruct;
	
typedef struct {
	double north;
	double east;
} NEDstruct; 


typedef struct {
	double surge;
	double sway;
} BODstruct;

NEDstruct Geo2NED(double lat, double lon, double latref, double lonref)
{
/* 
	This function converts geodetic (GPS) coordinates in degrees to NED in meters
	
	Input, lat - latitude in degrees
	Input, lon - longitude in degrees
	Input, latref - reference point coord
	Input, lonref - reference point coord
	
	Output, NED - NED position wrt to reference point
*/

	double Rne[3][3];
	double Pned[2];
	unsigned long long Rea = 6378137; //radius of earth in m
	float e = 0.08181919;
	
	NEDstruct NED;
	
	//printf("Current Pose (%f, %f)\n", lat, lon);
	//printf("Referen Pose (%f, %f)\n", latref, lonref);
	
	//printf("Rea = %llu e = %f \n", Rea, e);
	
	//convert degrees to radians
	lat = lat*pi/180;
	lon = lon*pi/180;
	latref = latref*pi/180;
	lonref = lonref*pi/180;
	
	//imperfect ellipsoid
	double Ne = Rea/(sqrt((1-pow(e,2)*pow(sin(lat),2))));
	double Neref = Rea/(sqrt((1-pow(e,2)*pow(sin(latref),2))));
	
	
	//printf("Ne = %f  Neref = %f \n", Ne, Neref);
	
	//ECEF coordinates
	double Pe_xe = Ne*cos(lat)*cos(lon);
	double Pe_ye = Ne*cos(lat)*sin(lon);
	double Pe_ze = Ne*(1-pow(e,2))*sin(lat);
	
	//printf("Pe_xe = %f  Pe_ye = %f \n", Pe_xe, Pe_ye);
	
	double Peref_xe =  Ne*cos(latref)*cos(lonref);
	double Peref_ye =  Ne*cos(latref)*sin(lonref);
	double Peref_ze = Ne*(1-pow(e,2))*sin(latref);
	
	//printf("Peref_xe = %f  Peref_ye = %f \n", Peref_xe, Peref_ye);
	
	//printf("Pexdif = %f  Peydif = %f \n", (Pe_xe - Peref_xe), (Pe_ye - Peref_ye));
	
	//Rne transformation matrix
	Rne[0][0] = -1*sin(latref)*cos(lonref);
	Rne[0][1] = -1*sin(latref)*sin(lonref);
	Rne[0][2] = cos(latref);
	Rne[1][0] = -1*sin(lonref);
	Rne[1][1] = cos(lonref);
	Rne[1][2] = 0;
	Rne[2][0] = -1*cos(latref)*cos(lonref);
	Rne[2][1] = -1*cos(latref)*sin(lonref);
	Rne[2][2] = -1*sin(latref);
	
	//NED coordinates
	Pned[0] = (Pe_xe - Peref_xe)*Rne[0][0] + (Pe_ye - Peref_ye)*Rne[0][1] + (Pe_ze - Peref_ze)*Rne[0][2];
	Pned[1] = (Pe_xe - Peref_xe)*Rne[1][0] + (Pe_ye - Peref_ye)*Rne[1][1] + 0;
	
	//printf("Pned (%f, %f)\n", Pned[0], Pned[1]);
	
	NED.north = Pned[0];
	NED.east = Pned[1];
	
	return NED;
}

BODstruct Geo2Bod(NEDstruct pose, float yaw)
{
/*
	This function calculates a transformation between geographic and body-fixed coordinate systems
	
	Input, pose - structure of pose.north pose.east to convert to body-fixed
	Input, yaw - current vehicle heading wrt to North in radians

	Output, body - body-fixed transformation
*/

	BODstruct body;

	body.surge = pose.north*cos(yaw)+pose.east*sin(yaw);
	body.sway = -pose.north*sin(yaw)+pose.east*cos(yaw);

	return body;
}

NEDstruct Bod2Geo(BODstruct body, float yaw)
{
/*
	This function calculates a transformation between body-fixed and geographic coordinate systems
	
	Input, body - structure of body.surge body.sway to convert to geographic
	Input, yaw - current vehicle heading wrt to North in radians

	Output, pose - body-fixed transformation
*/

	NEDstruct pose;

	pose.north = body.surge*cos(yaw)-body.sway*sin(yaw);
	pose.east =  body.surge*sin(yaw)+body.sway*cos(yaw);

	return pose;
}

	//////////
	//TIMING//
	//////////

long double GetTimeStamp()
{
/*
	This function grabs a timestamp of current kernel time at seconds since the epoch.

	Output, timestamp - fractional time since epoch in microsecond precision.
*/
	struct timeval tv;
	long double timestamp;
	
	gettimeofday(&tv,NULL);
	timestamp = (1000000.0*tv.tv_sec+tv.tv_usec)/1000000.0;
	
	return timestamp;
}

	//////////////////
	//MATH FUNCTIONS//
	//////////////////

float Saturation(float var, float min, float max)
{
/*
	This function acts like a saturation function
	
	Input, var - variable that may be saturated
	Input, min - minimum value
	Input, max - maximum value

	Output, sat_var - saturated variable
*/
	
	float sat_var;
	
	if( var > max) sat_var = max;
	else if( var < min) sat_var = min;
	else sat_var = var;
	
	return sat_var;
}

  ////////////////////////
  //GLOBAL LCM FUNCTIONS//
  ////////////////////////
  
int LCMCheckForMessage(lcm_t * lcm, struct timeval * timeout)
{
/*

	This function checks for an LCM message dependent on the instance 'lcm' for length timeout.
	Returns 1 if there is a message, or 0 if there isn't
	
	Input, lcm - lcm_t instance to check of on the channel
	Input, timeout - pointer to timeval structure to check for length of structure
	
	Output, status - 1 if there is a message, 0 if there is no message

*/

	int status, fileno;
	
	// check for LCM message
	fileno = lcm_get_fileno(lcm);
	
	fd_set fd;
	FD_ZERO(&fd);
	FD_SET(fileno, &fd);
  
	status = select(fileno + 1 , &fd,0,0, timeout);
	if(!status)
		return 0;
	else
		status = FD_ISSET(fileno, &fd);
	
	return status;
}