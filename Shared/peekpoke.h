#include<sys/types.h>
#include<sys/mman.h>
#include<fcntl.h>

int peekpoke(char write, unsigned int bits, off_t addr, unsigned int intval) {
/*
	This function is a modified version of peekpoke, which can be called in an executable.
	
	Input, write = Binary entry Read (0) or Write (1)
	Input, bits = number of bits associated with register (usually 32)
	Input, addr = address of register to write or read from
	Input, intval = value to write to register (ignored if read)
	
	Output, intval = Read data from register if in READ mode, or data written to register in WRITE mode.

*/
  // printf("peekpoke\n");
  off_t page;
  int fd;
  unsigned char *start;
  unsigned char *chardat, charval;
  unsigned short *shortdat, shortval; 
  unsigned int *intdat;

  // open memory block
  fd = open("/dev/mem", O_RDWR|O_SYNC);
 
  // error check for file open
  if (fd == -1) {
    perror("open(/dev/mem):");
    return 0;
  }
  
  page = addr & 0xfffff000;
  start = mmap(0, getpagesize(), PROT_READ|PROT_WRITE, MAP_SHARED, fd, page);
  
  // error check
  if (start == MAP_FAILED) {
    perror("mmap:");
    return 0;
  }

  // check to see number of bits
  if (bits == 8) {
    charval = (unsigned char)intval;
    chardat = start + (addr & 0xfff);
    if (write == 1) {
      *chardat = charval;
    }
    else if (!write) {
      intval = (unsigned int)*chardat;
    }
	else {
	printf("Wrong Mode\n");
	return write;
	}
  }	
  
  else if (bits == 16) {
    shortval = (unsigned short)intval;
    shortdat = (unsigned short *)(start + (addr & 0xfff));
    if (write == 1) {
      *shortdat = shortval;
    }
    else if (!write) {
      intval = (unsigned int)*shortdat;
    }
	else {
	printf("Wrong Mode\n");
	return write;
	}
  } 
  
  else { // bits == 32
    intdat = (unsigned int *)(start + (addr & 0xfff));
    if (write == 1) {
      *intdat = intval;
    }
    else if (!write){
      intval = *intdat;
    }
	else {
	printf("Wrong Mode\n");
	return write;
	}
  }
  
  close(fd);
  // printf("Peekpoking complete\n");
  return intval;
}

void portDinit () {
/*
	This function initializes the PORT D registers so that it is possible to treat the pins tied to the 
	RC line as GPIO input pins.
	
	No inputs -
	No outputs -
*/
peekpoke(1, 32, 0xE800003C, 0x0);
peekpoke(1, 32, 0xE800002C, 0x028006);
}

unsigned int readRadioActive() {
/*
	This function reads the register associated with the RC Active signal.
	
	No inputs -
	
	Returns:
	0 - RC
	1 - Auto
*/
return (peekpoke(0, 32, 0xE800001C, 0x0) & 0x4000U) >> 14;
}

unsigned int readRCKill() {
/*
	This function reads the register associated with the RC Kill signal.
	
	No inputs -
	
	Returns:
	0 - RC
	1 - Auto

*/
return (peekpoke(0, 32, 0xE800001C, 0x0) & 0x2000U) >> 13;
}
