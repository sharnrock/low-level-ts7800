/*
	Original Author: Ivan Bertaska
	Last Updated by: Ivan Bertaska
	Last Updated: 07/10/2014
	
	This file is used to transmit vehicle state information to the planner and controller.
	
	LCM Channels:
	IN:
		COMPASS
		GPS
		IMU
		MOTORS
	
	OUT:
		STATE
*/

# include <stdio.h>
# include <lcm/lcm.h>
# include <sys/select.h>
# include <unistd.h>
# include <string.h>
# include <math.h>
# include <time.h>
# include <libconfig.h>

//LCM header files
# include "gps_gps_t.h"
# include "imu_imu_t.h"
# include "RXimu_RXimu_t.h"
# include "compass_compass_t.h"
# include "RXmotors_RXmotors_t.h"
# include "RXState_RXState_t.h"

// local includes
# include "global.h"
# include "RXState.h"
# include "peekpoke.h"

int main()
{
	// initialization

	// lcm variables
	compass_compass_t compass_Struct;
	RXState_RXState_t state_Struct;
	RXmotors_RXmotors_t motors_Struct; 
	
	
	// depending on which IMU we are using
	#ifdef _XSENS
		imu_imu_t xsensIMU_struct;
		gps_gps_t gps_Struct;
	#endif
	
	#ifdef _SBG
		RXimu_RXimu_t sbgIMU_struct;
	#endif

	// geo2ned conversion
	NEDstruct gpsNED;
	
	// timestamp variables
    long double orig, curr, start;
    double sendtime;
	
	// timing variables
	struct timeval timeout = {0,5}; // wait for 5 milliseconds for message
	//struct timeval longtimeout = {5,0}; // wait for 5 seconds
	int msg = 0;
	
	// filename
	const char * filename = "Config/RXContConfig.txt";
	float Ts;
	int tmp = 0;
	
	// RC pin variables
	char rcActive, rcKill;
	
	/////////////////////
	//STARTUP PRINTOUTS//
	/////////////////////
	
	printf("\n\n\tFLORIDA ATLANTIC UNVIERSTIY (c) 2014\n");
	printf("\tROBOTX CHALLENGE - SINGAPORE BAY - 2014\n\n");
	
	printf("\tSTATE UPDATER START UP SCRIPT \n\n\n");
	#ifdef _DEBUG
	printf("\tDEBUG: ON\n");
	#endif 
	#ifndef _DEBUG
	printf("\tDEBUG: OFF\n");
	#endif
	#ifdef _SBG
	printf("\tIMU: SBG\n\n\n");
	#endif
	#ifdef _XSENS
	printf("\tIMU: XSENS\n\n\n");
	#endif
	
	////////////////////
	//READ CONFIG FILE//
	////////////////////
	
	if(tmp = RXStateFileRead(filename, &Ts))
		printf("\n\n\tSAMPLE PERIOD TS = %f\n", Ts);
	else 
	{	
		printf("\n\n\tNO SAMPLE PERIOD SPECIFIED IN CONFIG FILE OR INVALID FORMAT. EXITING FILE.\n");
		return -1;
	}
	
	///////////////////////////////////////////
	//INITIALIZE PORT D FOR READING GPIO PINS//
	///////////////////////////////////////////
	
	// initialize port D
	portDinit();
	
	/////////////////////
	//OPEN LCM CHANNELS//
	/////////////////////
	
	// compass
	compass_compass_t_subscription_t * LCMcompassPtr;
	lcm_t * LCMcompass = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!LCMcompass)
 	{
		printf("\tLCM COMPASS channel failure\n");
        return 1;
	}
	
	LCMcompassPtr = compass_compass_t_subscribe(LCMcompass, "COMPASS", &compass_handler, &compass_Struct);
	compass_compass_t_subscription_set_queue_capacity(LCMcompassPtr, 1); // set to only hold one value in buffer
	
	// motors
	RXmotors_RXmotors_t_subscription_t * LCMmotorsPtr;
	lcm_t * LCMmotors = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if(!LCMmotors)
 	{
		printf("\tLCM COMPASS channel failure\n");
        return 1;
	}
	
	LCMmotorsPtr = RXmotors_RXmotors_t_subscribe(LCMmotors, "MOTORS", &motors_handler, &motors_Struct);
	RXmotors_RXmotors_t_subscription_set_queue_capacity(LCMmotorsPtr, 1); // set to only hold one value in buffer
	
	#ifdef _XSENS
	// gps
	gps_gps_t_subscription_t * LCMgpsPtr;
	lcm_t * LCMgps = lcm_create("udpm://239.255.76.67:7669?ttl=1");
	if( !LCMgps)
    {
		printf("\tLCM GPS channel failure\n");
        return 1;
	}
	
	LCMgpsPtr = gps_gps_t_subscribe(LCMgps, "GPS", &gps_handler, &gps_Struct);
	gps_gps_t_subscription_set_queue_capacity(LCMgpsPtr, 1); // set to only hold one value in buffer
	
	// imu
	imu_imu_t_subscription_t * LCMimuPtr;
	lcm_t * LCMimu = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if( !LCMimu)
    {
		printf("\tLCM IMU channel failure\n");
        return 1;
	}
	
	LCMimuPtr = imu_imu_t_subscribe(LCMimu, "IMU", &xsens_handler, &xsensIMU_struct);
	imu_imu_t_subscription_set_queue_capacity(LCMimuPtr, 1); // set to only hold one value in buffer
	#endif
	
	#ifdef _SBG
	RXimu_RXimu_t_subscription_t * LCMimuPtr;
	lcm_t * LCMimu = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if( !LCMimu)
    {
		printf("\tLCM IMU channel failure\n");
        return 1;
	}
	
	LCMimuPtr = RXimu_RXimu_t_subscribe(LCMimu, "IMU", &sbg_handler, &sbgIMU_struct);
	RXimu_RXimu_t_subscription_set_queue_capacity(LCMimuPtr, 1); // set to only hold one value in buffer
	#endif
	
	// lcm for state message transmission
	lcm_t * LCMState = lcm_create("udpm://239.255.76.67:7667?ttl=1");
	if( !LCMState)
    {
		printf("\tLCM STATE channel failure\n");
        return 1;
	}
	
	printf("\tOPENED LCM CHANNELS SUCCESSFULLY\n\n");
	printf("\tWAITING FOR LCM CHANNEL MESSAGES\n\n");
	
	// handle each channel individually
	printf("\tCOMPASS: \n");
	lcm_handle(LCMcompass);
	printf("\tCOMPASS: SUCCESS \n\n");
	
	#ifdef _XSENS
	printf("\tGPS: \n");
	lcm_handle(LCMgps);
	printf("\tGPS: SUCCESS \n\n");
	#endif

	printf("\tIMU: \n");
	lcm_handle(LCMimu);
	printf("\tIMU: SUCCESS \n\n");
	
	printf("\tMOTORS: \n");
	lcm_handle(LCMmotors);
	printf("\tMOTORS: SUCCESS \n\n");
	
	// get timestamp
	start = GetTimeStamp();
	
	
	// convert geodetic to NED
	#ifdef _XSENS
	gpsNED = Geo2NED(gps_Struct.latitude, gps_Struct.longitude, LATREF, LONREF);
	#endif
	#ifdef _SBG
	gpsNED = Geo2NED(sbgIMU_struct.Pos[0], sbgIMU_struct.Pos[1], LATREF, LONREF);
	#endif
	
	// store in proper structure
	state_Struct.USV_pose[0] = gpsNED.north;
	state_Struct.USV_pose[1] = gpsNED.east;
	state_Struct.USV_pose[2] = compass_Struct.heading;
	
	#ifdef _XSENS
	state_Struct.USV_vel[0] = xsensIMU_struct.VelX;
	state_Struct.USV_vel[1] = xsensIMU_struct.VelY;
	
	state_Struct.USV_euler[0] = xsensIMU_struct.roll;
	state_Struct.USV_euler[1] = xsensIMU_struct.pitch;
	#endif
	
	#ifdef _SBG
	state_Struct.USV_vel[0] = sbgIMU_struct.Vel[0];
	state_Struct.USV_vel[1] = sbgIMU_struct.Vel[1];
	
	state_Struct.USV_euler[0] = sbgIMU_struct.Eul[0];
	state_Struct.USV_euler[1] = sbgIMU_struct.Eul[1];
	#endif
	
	// compass
	state_Struct.USV_euler[2] = compass_Struct.heading;
	
	// motors
	state_Struct.PortState[0] = (float)motors_Struct.PortState[0];
	state_Struct.PortState[1] = (float)motors_Struct.PortState[1];
	state_Struct.StbdState[0] = (float)motors_Struct.StbdState[0];
	state_Struct.StbdState[1] = (float)motors_Struct.StbdState[1];
	
	// timing
	state_Struct.StartTime = (double)start;
	state_Struct.ElapsedTime = 0.0;
	
	#ifdef _DEBUG
	printf("\tRECEIVED MESSAGE FROM SENSORS - CURRENT DATA: \n");
	printf("\tPOSE: \n");
	printf("\t[\t%f\t%f\t%f\t]\n",state_Struct.USV_pose[0], state_Struct.USV_pose[1],state_Struct.USV_pose[2]);
	printf("\tVELOCITIES: \n");
	printf("\t[\t%f\t%f\t]\n", state_Struct.USV_vel[0], state_Struct.USV_vel[1]);
	printf("\tEULER ANGLES: \n");
	printf("\t[\t%f\t%f\t%f\t]\n", state_Struct.USV_euler[0], state_Struct.USV_euler[1],state_Struct.USV_euler[2]);
	printf("\tMOTORS COMMANDS: \n");
	printf("\t[\t%f\t%f\t%f\t%f\t]\n", state_Struct.PortState[0], state_Struct.PortState[1],state_Struct.StbdState[0],state_Struct.StbdState[1]);
	printf("\tTIMESTAMP: \n");
	printf("\t%f\n\n", state_Struct.ElapsedTime);
	#endif
	
	// publish message
	RXState_RXState_t_publish(LCMState, "STATE", &state_Struct);
	
	printf("\tINITIAL MESSAGE PUBLISHED TO 'STATE' CHANNEL \n\n\n");
	
	orig = GetTimeStamp();
	while(1)
	{
		// check for compass
		msg = LCMCheckForMessage(LCMcompass, &timeout);
		if(msg == 1)
		{
			lcm_handle(LCMcompass);
			msg = 0;
		}
		
		#ifdef _XSENS
		msg = LCMCheckForMessage(LCMgps, &timeout);
		if(msg == 1)
		{
			lcm_handle(LCMgps);
			msg = 0;
		}
		#endif
		
		msg = LCMCheckForMessage(LCMimu, &timeout);
		if(msg == 1)
		{
			lcm_handle(LCMimu);
			msg = 0;
		}
		
		msg = LCMCheckForMessage(LCMmotors, &timeout);
		if(msg == 1)
		{
			lcm_handle(LCMmotors);
			msg = 0;
		}
		
		// get timer
		curr = GetTimeStamp();
		sendtime = (double)(curr - orig);
		
		if(sendtime > Ts)
		{
			// check data lines
			rcActive = readRadioActive();
			rcKill = readRCKill();
		
			// transfer geo to NED for GPS channel
			#ifdef _XSENS
			gpsNED = Geo2NED(gps_Struct.latitude, gps_Struct.longitude, LATREF, LONREF);
			#endif
			#ifdef _SBG
			gpsNED = Geo2NED(sbgIMU_struct.Pos[0], sbgIMU_struct.Pos[1], LATREF, LONREF);
			#endif
			
			// store in proper structure
			state_Struct.USV_pose[0] = gpsNED.north;
			state_Struct.USV_pose[1] = gpsNED.east;
			state_Struct.USV_pose[2] = compass_Struct.heading;
			
			#ifdef _XSENS
			state_Struct.USV_vel[0] = xsensIMU_struct.VelX; // surge
			state_Struct.USV_vel[1] = xsensIMU_struct.VelY; // sway
			state_Struct.USV_vel[2] = -1*xsensIMU_struct.GyrZ; // yaw rate (corrected for body fixed coordinate system)
			
			state_Struct.USV_euler[0] = xsensIMU_struct.roll;
			state_Struct.USV_euler[1] = xsensIMU_struct.pitch;
			
			state_Struct.GeoCoord[0] = gps_Struct.latitude;
			state_Struct.GeoCoord[1] = gps_Struct.longitude;
			#endif
			
			#ifdef _SBG
			state_Struct.USV_vel[0] = sbgIMU_struct.Vel[0];
			state_Struct.USV_vel[1] = sbgIMU_struct.Vel[1];
			state_Struct.USV_vel[2] = sbgIMU_struct.Gyr[2];
			
			state_Struct.USV_euler[0] = sbgIMU_struct.Eul[0];
			state_Struct.USV_euler[1] = sbgIMU_struct.Eul[1];
			
			state_Struct.GeoCoord[0] = sbgIMU_struct.Pos[0];
			state_Struct.GeoCoord[1] = sbgIMU_struct.Pos[1];
			#endif
			
			// compass
			state_Struct.USV_euler[2] = compass_Struct.heading;
			
			// motors
			state_Struct.PortState[0] = (float)motors_Struct.PortState[0];
			state_Struct.PortState[1] = (float)motors_Struct.PortState[1];
			state_Struct.StbdState[0] = (float)motors_Struct.StbdState[0];
			state_Struct.StbdState[1] = (float)motors_Struct.StbdState[1];
			
			// timing
			state_Struct.ElapsedTime = (double)(curr-start);
			
			// bit masked values
			state_Struct.RCmode = 1*rcActive + 2*rcKill;
			
			#ifdef _DEBUG
			printf("\tRECEIVED MESSAGE FROM SENSORS - CURRENT DATA: \n");
			printf("\tPOSE: \n");
			printf("\t[\t%f\t%f\t%f\t]\n",state_Struct.USV_pose[0], state_Struct.USV_pose[1],state_Struct.USV_pose[2]);
			printf("\tVELOCITIES: \n");
			printf("\t[\t%f\t%f\t%f\t]\n", state_Struct.USV_vel[0], state_Struct.USV_vel[1],state_Struct.USV_vel[2]);
			printf("\tEULER ANGLES: \n");
			printf("\t[\t%f\t%f\t%f\t]\n", state_Struct.USV_euler[0], state_Struct.USV_euler[1],state_Struct.USV_euler[2]);
			printf("\tMOTORS COMMANDS: \n");
			printf("\t[\t%f\t%f\t%f\t%f\t]\n", state_Struct.PortState[0], state_Struct.PortState[1],state_Struct.StbdState[0],state_Struct.StbdState[1]);
			printf("\tTIMESTAMP: \n");
			printf("\t%f\n",state_Struct.ElapsedTime);
			printf("\tRC PINS: \n");
			printf("\t%u\t%u\n\n",rcKill, rcActive);
			#endif
	
			
			// publish to LCM channel
			RXState_RXState_t_publish(LCMState, "STATE", &state_Struct);
			
			// reset timer
			orig = GetTimeStamp();
		}
		
	}

	return -1;
}