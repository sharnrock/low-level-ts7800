package RXState;

struct RXState_t
{
    float USV_pose[3]; // x,y, heading in NED
    float USV_vel[3]; // surge, sway, and yaw
    float USV_euler[3]; // roll, pitch, yaw

    float PortState[2]; // Normalized Motor Command, Normalized Azimuth Command
    float StbdState[2]; // Normalized Motor Command, Normalized Azimuth Command

    double StartTime; // Unix timestamp
    double ElapsedTime; // seconds

    int8_t RCmode; // bit masked RCmode

    // bit masked RC
    // 0 == OFF/RC
    // 1 == OFF/Auto
    // 2 == ON/RC
    // 3 == ON/Auto 
}