/////////////////////////////
//CONFIG FILE READ FUNCTION//
/////////////////////////////

int RXStateFileRead(const char * config_file_name, float * Ts)
{
	config_t cfg;               /*Returns all parameters in this structure */
    config_setting_t *setting;
	double ftmp;
 
    // Initialization
    config_init(&cfg);
	
    // Read the file. If there is an error, report it and exit.
    if (!config_read_file(&cfg, config_file_name))
    {
        printf("\n\n\tRX CONFIG FILE READ PROBLEM.\n\n");
        config_destroy(&cfg);
        return 0;
    }
	
	if (config_lookup_float(&cfg, "Ts", &ftmp))
		*(Ts) = (float)ftmp;
    else
        printf("\n\tNo 'Ts' setting in configuration file.");
		
	return 1;
}



/////////////////
//LCM FUNCTIONS//
/////////////////

static void compass_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const compass_compass_t * msg, void * user_data)
{/*
	This function is used as a handler for the 'COMPASS'channel on LCM.
	
	Traditional LCM Inputs:
	
	Input, rbuf - LCM receive buffer
	Input, channel - channel name
	Input, msg - msg received
	Input, user_data - should be same type as msg, transfer all data from this handler to function
*/

	memcpy(user_data,msg,sizeof(compass_compass_t));
}

static void gps_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const gps_gps_t * msg, void * user_data)
{/*
	This function is used as a handler for the 'GPS'channel on LCM.
	
	Traditional LCM Inputs:
	
	Input, rbuf - LCM receive buffer
	Input, channel - channel name
	Input, msg - msg received
	Input, user_data - should be same type as msg, transfer all data from this handler to function
*/
	
	memcpy(user_data,msg,sizeof(gps_gps_t));
}

static void xsens_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const imu_imu_t * msg, void * user_data)
{/*
	This function is used as a handler for the 'IMU'channel on LCM.
	
	Traditional LCM Inputs:
	
	Input, rbuf - LCM receive buffer
	Input, channel - channel name
	Input, msg - msg received
	Input, user_data - should be same type as msg, transfer all data from this handler to function
*/
	memcpy(user_data,msg,sizeof(imu_imu_t));
}

static void sbg_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const RXimu_RXimu_t * msg, void * user_data)
{/*
	This function is used as a handler for the 'IMU'channel on LCM.
	
	Traditional LCM Inputs:
	
	Input, rbuf - LCM receive buffer
	Input, channel - channel name
	Input, msg - msg received
	Input, user_data - should be same type as msg, transfer all data from this handler to function
*/
	memcpy(user_data,msg,sizeof(RXimu_RXimu_t));
}

static void motors_handler(const lcm_recv_buf_t *rbuf, const char * channel, 
        const RXmotors_RXmotors_t * msg, void * user_data)
{/*
	This function is used as a handler for the 'IMU'channel on LCM.
	
	Traditional LCM Inputs:
	
	Input, rbuf - LCM receive buffer
	Input, channel - channel name
	Input, msg - msg received
	Input, user_data - should be same type as msg, transfer all data from this handler to function
*/
	memcpy(user_data,msg,sizeof(RXmotors_RXmotors_t));
}